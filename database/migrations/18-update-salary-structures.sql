ALTER TABLE salary_structures
RENAME salary TO basic_salary;

ALTER TABLE salary_structures
ADD COLUMN salary_structure_name TEXT UNIQUE NOT NULL,
ADD COLUMN hra REAL,
ADD COLUMN tax REAL,
ADD COLUMN extra REAL;

ALTER TABLE salary_structures
ALTER COLUMN basic_salary TYPE REAL;