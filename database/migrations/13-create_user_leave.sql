CREATE TYPE valid_leave_day AS ENUM ('half_day', 'full_day');

CREATE TYPE valid_approval AS ENUM ('pending', 'approved');

CREATE OR REPLACE FUNCTION trigger_set_timestamp()
RETURNS TRIGGER AS $$
BEGIN
  NEW.updated_at = NOW();
  RETURN NEW;
END;
$$ LANGUAGE 'plpgsql';

CREATE TABLE user_leave (
    id SERIAL NOT NULL PRIMARY KEY,
    user_id INT REFERENCES users(id) NOT NULL,
    leave_id INT REFERENCES leaves(id) NOT NULL,
    from_date DATE NOT NULL,
    to_date DATE NOT NULL,
    reason TEXT,
    leave_day valid_leave_day NOT NULL,
    approval valid_approval NOT NULL,
    approver_id INT REFERENCES approvers(id) NOT NULL,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW()
);

CREATE TRIGGER set_timestamp
BEFORE UPDATE ON user_leave
FOR EACH ROW
EXECUTE PROCEDURE trigger_set_timestamp();