CREATE TYPE valid_gender AS ENUM ('male', 'female');

CREATE OR REPLACE FUNCTION trigger_set_timestamp()
RETURNS TRIGGER AS $$
BEGIN
  NEW.updated_at = NOW();
  RETURN NEW;
END;
$$ LANGUAGE 'plpgsql';

CREATE TABLE users (
    id SERIAL NOT NULL PRIMARY KEY,
    salutation VARCHAR(4),
    first_name VARCHAR(64) NOT NULL,
    middle_name VARCHAR(64),
    last_name VARCHAR(64),
    user_email VARCHAR(128) NOT NULL UNIQUE,
    user_address TEXT,
    phone_number VARCHAR(15),
    date_of_birth DATE,
    user_password VARCHAR(256) NOT NULL,
    company_id INT REFERENCES companies(id) ON DELETE SET NULL,
    gender valid_gender,
    date_of_joining DATE,
    employment_type_id INT REFERENCES employment_types(id),
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW()
);

CREATE TRIGGER set_timestamp
BEFORE UPDATE ON users
FOR EACH ROW
EXECUTE PROCEDURE trigger_set_timestamp();