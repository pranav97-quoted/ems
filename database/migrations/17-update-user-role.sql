ALTER TABLE user_role
DROP CONSTRAINT user_role_user_id_fkey,
ADD CONSTRAINT user_role_user_id_fkey
FOREIGN KEY (user_id)
REFERENCES users(id)
ON DELETE CASCADE;