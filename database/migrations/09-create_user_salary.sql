CREATE OR REPLACE FUNCTION trigger_set_timestamp()
RETURNS TRIGGER AS $$
BEGIN
  NEW.updated_at = NOW();
  RETURN NEW;
END;
$$ LANGUAGE 'plpgsql';

CREATE  TABLE user_salary (
    id SERIAL NOT NULL PRIMARY KEY,
    user_id INT REFERENCES users(id) NOT NULL,
    user_salary_struct_id INT REFERENCES user_salary_structure(id) NOT NULL,
    salary_date DATE NOT NULL,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW()
);

CREATE TRIGGER set_timestamp
BEFORE UPDATE ON user_salary
FOR EACH ROW
EXECUTE PROCEDURE trigger_set_timestamp();