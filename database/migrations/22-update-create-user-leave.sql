ALTER TABLE user_leave
DROP COLUMN to_date;

ALTER TABLE user_leave
RENAME COLUMN from_date TO leave_date;

ALTER TABLE user_leave
ALTER COLUMN approval SET DEFAULT 'pending';

ALTER TABLE user_leave
ALTER COLUMN half_day_date DROP NOT NULL;

ALTER TABLE user_leave
DROP CONSTRAINT user_leave_approver_id_fkey,
ADD CONSTRAINT user_leave_approver_id_fkey
FOREIGN KEY (approver_id)
REFERENCES users(id)
ON DELETE CASCADE;

ALTER TABLE user_leave
DROP COLUMN half_day_date;