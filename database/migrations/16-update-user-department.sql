ALTER TABLE user_department
DROP CONSTRAINT user_department_user_id_fkey,
ADD CONSTRAINT user_department_user_id_fkey
FOREIGN KEY (user_id)
REFERENCES users(id)
ON DELETE CASCADE;