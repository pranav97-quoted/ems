CREATE OR REPLACE FUNCTION trigger_set_timestamp()
RETURNS TRIGGER AS $$
BEGIN
  NEW.updated_at = NOW();
  RETURN NEW;
END;
$$ LANGUAGE 'plpgsql';

CREATE TABLE approvers (
    id SERIAL NOT NULL PRIMARY KEY,
    user_id INT REFERENCES users(id) NOT NULL,
    approver_id INT REFERENCES users(id) NOT NULL,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW()
);

CREATE TRIGGER set_timestamp
BEFORE UPDATE ON approvers
FOR EACH ROW
EXECUTE PROCEDURE trigger_set_timestamp();