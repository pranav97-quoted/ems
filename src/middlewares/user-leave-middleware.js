const moment = require('moment');
const { executeQuery, knex } = require('../providers/db-connections.js');

exports.isAnyLeaveAssignedToUser = (request, response, next) => {
  let isAnyLeaveAssignedToUserQuery = knex('user_leave').select(knex.raw("TO_CHAR(leave_date :: DATE, 'yyyy-mm-dd') AS leave_date"), 'leave_day').where('user_id', request.params.uid);
  if (request.body.start_date && request.body.end_date) {
    isAnyLeaveAssignedToUserQuery = isAnyLeaveAssignedToUserQuery
      .where('leave_date', '>=', moment(request.body.start_date).utc().format('YYYY-MM-DD')).andWhere('leave_date', '<', moment(request.body.end_date).utc().format('YYYY-MM-DD'));
  }

  if (request.body.half_day_date && moment(request.body.start_date).utc().isSame(moment(request.body.half_day_date).utc()) && moment(request.body.end_date).utc().diff(moment(request.body.start_date).utc(), 'days') === 1) {
    isAnyLeaveAssignedToUserQuery = isAnyLeaveAssignedToUserQuery.andWhere('leave_date', moment(request.body.half_day_date).utc().format('YYYY-MM-DD'));
  }
  executeQuery(isAnyLeaveAssignedToUserQuery)
    .then((userLeave) => {
      if (userLeave && userLeave.rows && userLeave.rows.length) {
        userLeave.rows
          .forEach((row, idx, arr) => {
            if (row.leave_day === 'half_day' && request.body.leave_type === 'half_day') {
              if (idx === arr.length - 1) {
                next();
              } else {
                return;
              }
            } else if (row.leave_day === 'full_day') {
              return response.status(400)
                .json({
                  errorType: 'LEAVE_ALREADY_ASSIGNED',
                  errorMessage: `Leave already assigned to the user between '${moment(request.body.start_date).format('YYYY-MM-DD')}' AND '${moment(request.body.end_date).format('YYYY-MM-DD')}'`,
                });
            } else if (row.leave_day === 'half_day' && request.body.leave_type === 'full_day') {
              if (idx === arr.length - 1) {
                next();
              } else {
                return;
              }
            }
          });
      } else {
        next();
      }
    });
};

exports.isUserLeaveExistsById = (request, response, next) => {
  const isLeaveExistsByDateQuery = knex('user_leave')
    .select()
    .where('id', request.params.ulid)
    .andWhere('user_id', request.params.uid)
    .limit(1);
  executeQuery(isLeaveExistsByDateQuery)
    .then((userLeave) => {
      if (userLeave && userLeave.rows && userLeave.rows.length) {
        next();
      } else {
        response.status(404)
          .json({
            errorType: 'USER_LEAVE_NOT_FOUND',
            errorMessage: `User leave does not exits for the date ${request.body.leave_date}`,
          });
      }
    });
};

exports.isLeaveInPendingState = (request, response, next) => {
  const isLeaveInPendingStateQuery = knex('user_leave')
    .select('approval')
    .where('id', request.params.ulid)
    .limit(1);
  executeQuery(isLeaveInPendingStateQuery)
    .then((userLeave) => {
      if (userLeave && userLeave.rows[0].approval === 'pending') {
        next();
      } else {
        response.status(400)
          .json({
            errorType: 'LEAVE_STATUS_APPROVED',
            errorMessage: 'You cannot update leave as it is already approved',
          });
      }
    });
};
