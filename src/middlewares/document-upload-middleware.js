const multer = require('multer');
const path = require('path');
const mkdirp = require('mkdirp');

const { executeQuery, knex } = require('../providers/db-connections.js');

exports.fileUpload = (req, res, next) => {
  const storage = multer.diskStorage({
    destination: (req, file, callback) => {
      mkdirp(`./static/users/${req.params.uid}/`).then(() => {
        console.log('Made directories');
      });
      callback(null, `./static/users/${req.params.uid}`);
    },
    filename (req, file, cb) {
      const fileName = file.originalname.split('.')[0];
      cb(null, `${fileName}-${Date.now()}${path.extname(file.originalname)}`);
    },
  });

  const upload = multer({
    storage,
  }).single('userDoc');

  upload(req, res, (err) => {
    if (err) {
      res.status(400)
        .json({
          errorType: 'FILE_UPLOAD_FAILED',
          errorMessage: 'Error uploading file',
        });
    } else {
      next();
    }
  });
};

exports.checkDocumentExistsById = (request, response, next) => {
  const checkDocumentExistsByIdQuery = knex('user_document')
    .select()
    .where('id', request.params.doid)
    .andWhere('user_id', request.params.uid)
    .limit(1);
  executeQuery(checkDocumentExistsByIdQuery)
    .then((document) => {
      if (document && document.rows && document.rows.length) {
        next();
      } else {
        response.status(404)
          .json({
            errorType: 'FILE_NOT_FOUND',
            errorMessage: `Document having id '${request.params.doid}' does not exists`,
          });
      }
    });
};
