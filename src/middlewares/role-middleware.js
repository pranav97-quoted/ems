const { executeQuery, knex } = require('../providers/db-connections.js');

exports.checkRoleExistsById = (request, response, next) => {
  const checkRoleByIdQuery = knex('roles')
    .select('role_type')
    .where('id', request.params.rid || request.body.role_id)
    .limit(1);
  if (!(request.body.role_id)) {
    executeQuery(checkRoleByIdQuery).then((role) => {
      if (role && role.rows.length) {
        if (role.rows[0].role_type === 'admin') {
          response.status(400)
            .json({
              errorType: 'CANNOT_DELETE_OR_UPDATE_ROLE',
              errorMessage: `Role 'admin' cannot be deleted nor can be updated`,
            });
        } else {
          next();
        }
      }
    });
  } else {
    executeQuery(checkRoleByIdQuery)
      .then((role) => {
        if (role && role.rows.length) {;
          next();
        } else {
          response.status(404)
            .json({
              errorType: 'ROLE_NOT_FOUND',
              errorMessage: `Role having id '${request.params.rid || request.body.role_id}' does not exists`,
            });
        }
      });
  }
};
