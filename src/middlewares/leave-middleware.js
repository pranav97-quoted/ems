const { executeQuery, knex } = require('../providers/db-connections.js');

exports.checkLeaveExistsById = (request, response, next) => {
  if (request.params.lid || request.body.leave_id) {
    const checkLeaveExistsByIdQuery = knex('leaves')
      .select('leave_type')
      .where('id', request.params.lid || request.body.leave_id)
      .limit(1);
    executeQuery(checkLeaveExistsByIdQuery)
      .then((leave) => {
        if (leave && leave.rows.length) {
          next();
        } else {
          response.status(404)
            .json({
              errorType: 'LEAVE_NOT_FOUND',
              errorMessage: `Leave having id '${request.params.lid || request.body.leave_id}' does not exists`,
            });
        }
      });
  } else {
    next();
  }
};

exports.isLeaveAssignedToAnyUser = (request, response, next) => {
  const isLeaveAssignedToAnyUserQuery = knex('user_leave')
    .select()
    .where('leave_id', request.params.lid)
    .limit(1);
  executeQuery(isLeaveAssignedToAnyUserQuery)
    .then((assignedLeave) => {
      if (assignedLeave && assignedLeave.rows.length) {
        response.status(400)
          .json({
            errorType: 'LEAVE_ASSIGNED',
            errorMessage: 'Leave is already assigned to user(s). Kindly detach it in order to delete it',
          });
      } else {
        next();
      }
    });
};
