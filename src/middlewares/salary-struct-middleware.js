const { executeQuery, knex } = require('../providers/db-connections.js');

exports.checkSalaryStructExistsById = (request, response, next) => {
  const checkSalaryStructExistsByIdQuery = knex('salary_structures')
    .select('salary_structure_name')
    .where('id', request.params.sid || request.params.ssid)
    .limit(1);
  executeQuery(checkSalaryStructExistsByIdQuery)
    .then((salaryStructure) => {
      if (salaryStructure && salaryStructure.rows.length) {
        next();
      } else {
        response.status(404)
          .json({
            errorType: 'SALARY_STRUCTURE_NOT_FOUND',
            errorMessage: `Salary structure having id '${request.params.sid || request.params.ssid}' does not exists`,
          });
      }
    });
};

exports.isSalaryStructAssignedToUser = (request, response, next) => {
  const isSalaryStructAssignedToUserQuery = knex('user_salary_structure')
    .select('salary_structure_id')
    .where('salary_structure_id', request.params.sid)
    .limit(1);
  executeQuery(isSalaryStructAssignedToUserQuery)
    .then((userSalaryStructure) => {
      if (userSalaryStructure && userSalaryStructure.rows.length) {
        response.status(400)
          .json({
            errorType: 'SALARY_STRUCTURE_ASSIGNED',
            errorMessage: 'Salary structure is already assigned to user(s). Kindly detach it in order to delete it',
          });
      } else {
        next();
      }
    });
};
