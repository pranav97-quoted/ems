const { executeQuery, knex } = require('../providers/db-connections.js');

exports.checkUserExistsById = (request, response, next) => {
  const checkUserExistsByIdQuery = knex('users')
    .select('user_email')
    .where('id', request.params.uid);
  executeQuery(checkUserExistsByIdQuery)
    .then((user) => {
      if (user && user.rows && user.rows.length) {
        next();
      } else {
        response.status(400)
          .json({
            errorType: 'USER_NOT_FOUND',
            errorMessage: `User having id '${request.params.uid}' does not exists`,
          });
      }
    });
};

exports.checkUserExistsByEmailAndIsActive = (request, response, next) => {
  const checkUserByEmailQuery = knex('users')
    .select('id', 'is_active')
    .where('user_email', request.body.email)
    .limit(1);
  executeQuery(checkUserByEmailQuery)
    .then((user) => {
      if (user && user.rows.length && user.rows[0].id) {
        if (!(user.rows[0].is_active)) {
          next();
        } else {
          response.status(400).json({
            errorType: 'USER_ALREADY_ACTIVE',
            errorMessage: `User having email '${request.body.email}' is already an active user`,
          });
        }
      } else {
        response.status(404)
          .json({
            errorType: 'USER_NOT_FOUND',
            errorMessage: `User having email '${request.body.email}' does not exists`,
          });
      }
    });
};

exports.isEmailAndConfirmationCodeValid = (request, response, next) => {
  const checkEmailAndConfirmationCodeQuery = knex('users')
    .select('user_email')
    .where('user_email', request.body.email)
    .andWhere('confirmation_code', request.body.confirmation_code)
    .limit(1);
  executeQuery(checkEmailAndConfirmationCodeQuery)
    .then((user) => {
      if (user && user.rows.length) {
        next();
      } else {
        response.status(403)
          .json({
            errorType: 'INVALID_REQUEST',
            errorMessage: 'You don\'t have permission to access this resource',
          });
      }
    });
};

exports.checkUserExistsById = (request, response, next) => {
  const checkUserExistsByIdQuery = knex('users')
    .select('user_email')
    .where('id', request.params.uid)
    .limit(1);
  executeQuery(checkUserExistsByIdQuery)
    .then((user) => {
      if (user && user.rows && user.rows.length) {
        next();
      } else {
        response.status(400)
          .json({
            errorType: 'USER_NOT_FOUND',
            errorMessage: `User having id '${request.params.uid}' does not exists`,
          });
      }
    });
};

exports.isUserAdmin = (request, response, next) => {
  const isUserAdminQuery = knex('roles')
    .innerJoin('user_role', 'roles.id', '=', 'user_role.role_id')
    .innerJoin('users', 'user_role.user_id', '=', parseInt(request.params.uid, 10))
    .select('roles.role_type')
    .where('roles.role_type', 'admin')
    .limit(1);
  executeQuery(isUserAdminQuery)
    .then((adminUser) => {
      if (adminUser && adminUser.rows.length) {
        response.status(401)
          .json({
            errorType: 'UNAUTHORIZED_ACCESS',
            errorMessage: 'User having role \'admin\' cannot be deleted',
          });
      } else {
        next();
      }
    });
};
