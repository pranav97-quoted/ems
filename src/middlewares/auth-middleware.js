const { executeQuery, knex } = require('../providers/db-connections.js');

exports.checkAdminAuth = (request, response, next) => {
  const unauthorizedErrorObject = {
    errorType: 'UNAUTHORIZED_ACCESS',
    errorMessage: 'You cannot access to this endpoint',
  };
  const selectUserRoleQuery = knex('roles')
    .innerJoin('user_role', 'roles.id', '=', 'user_role.role_id')
    .innerJoin('users', 'user_role.user_id', '=', request.session.user.userId)
    .select('roles.role_type')
    .where('roles.role_type', 'admin')
    .limit(1);
  return executeQuery(selectUserRoleQuery)
    .then((userRole) => {
      if (userRole && userRole.rows[0]) {
        next();
      } else {
        response.status(401)
          .json(unauthorizedErrorObject);
      }
    });
};

exports.isUserAuthorized = (request, response, next) => {
  if (request.session.user) {
    next();
  } else {
    response.status(401)
      .json({
        errorType: 'UNAUTHORIZED_ACCESS',
        errorMessage: 'Please login to the system',
      });
  }
};
