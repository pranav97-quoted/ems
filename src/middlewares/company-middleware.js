const { executeQuery, knex } = require('../providers/db-connections.js');

exports.checkCompanyExistsById = (request, response, next) => {
  if (!(request.body.company_id)) {
    return next();
  }
  const checkCompanyByIdQuery = knex('companies')
    .select('company_name')
    .where('id', request.params.cid || request.body.company_id)
    .limit(1);
  executeQuery(checkCompanyByIdQuery).then((company) => {
    if (company && company.rows.length) {
      next();
    } else {
      response.status(404).json({
        errorType: 'COMPANY_NOT_FOUND',
        errorMessage: `Company having id '${request.params.cid || request.body.company_id}' does not exists`,
      });
    }
  });
};
