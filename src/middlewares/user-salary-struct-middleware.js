const moment = require('moment');
const { executeQuery, knex } = require('../providers/db-connections.js');

exports.isAnySalaryStructAssignedToUser = (request, response, next) => {
  const selectFromUserSalaryStructQuery = knex('user_salary_structure')
    .select()
    .where((queryBuilder) => {
      queryBuilder.where((subQueryBuilder) => {
        subQueryBuilder.where('salary_structure_start_time', '<=', new Date(request.body.start_time))
          .andWhere('salary_structure_end_time', '>', new Date(request.body.start_time))
          .andWhere('salary_structure_end_time', '<', new Date(request.body.end_time));
      })
        .orWhere((subQueryBuilder) => {
          subQueryBuilder.where('salary_structure_start_time', '>=', new Date(request.body.start_time))
            .andWhere('salary_structure_start_time', '<', new Date(request.body.end_time))
            .andWhere('salary_structure_end_time', '>', new Date(request.body.end_time));
        })
        .orWhere((subQueryBuilder) => {
          subQueryBuilder.where('salary_structure_start_time', '<', new Date(request.body.start_time))
            .andWhere('salary_structure_start_time', '<', new Date(request.body.end_time))
            .andWhere('salary_structure_end_time', '>', new Date(request.body.start_time))
            .andWhere('salary_structure_end_time', '>=', new Date(request.body.end_time));
        }).orWhere((subQueryBuilder) => {
          subQueryBuilder.where('salary_structure_start_time', '>', new Date(request.body.start_time))
            .andWhere('salary_structure_start_time', '<', new Date(request.body.end_time))
            .andWhere('salary_structure_end_time', '>', new Date(request.body.start_time))
            .andWhere('salary_structure_end_time', '<=', new Date(request.body.end_time));
        })
        .orWhere((subQueryBuilder) => {
          subQueryBuilder.where('salary_structure_start_time', '=', new Date(request.body.start_time)).andWhere('salary_structure_end_time', '=', new Date(request.body.end_time));
        });
    })
    .andWhere('user_id', parseInt(request.params.uid, 10))
    .limit(1);

  executeQuery(selectFromUserSalaryStructQuery)
    .then((userSalaryStructure) => {
      if (userSalaryStructure && userSalaryStructure.rows.length) {
        response.status(400)
          .json({
            errorType: 'SALARY_STRUCTURE_ALREADY_ASSIGNED',
            erorrMessage: `Cannot assign salary-structure as there is already a salary structure assigned to the user having id '${request.params.uid}' in-between '${moment(userSalaryStructure.rows[0].salary_structure_start_time).format('YYYY-MM-DD')}' and '${moment(userSalaryStructure.rows[0].salary_structure_end_time).format('YYYY-MM-DD')}'`,
          });
      } else {
        next();
      }
    });
};

exports.checkUserSalaryStructExistsById = (request, response, next) => {
  const checkUserSalaryStructExistsById = knex('user_salary_structure')
    .select()
    .where('id', request.params.ussid)
    .limit(1);
  executeQuery(checkUserSalaryStructExistsById)
    .then((userSalaryStructure) => {
      if (userSalaryStructure && userSalaryStructure.rows.length) {
        next();
      } else {
        response.status(404)
          .json({
            errorType: 'USER_SALARY_STRUCT_NOT_FOUND',
            errorMessage: `User salary structure having id '${request.params.ussid}' does not exists`,
          });
      }
    });
};

exports.checkUserSalaryStructAssignedToUser = (request, response, next) => {
  const checkUserSalaryStructAssignedToUserQuery = knex('user_salary_structure')
    .select()
    .where('id', request.params.ussid)
    .andWhere('user_id', request.params.uid)
    .limit(1);
  executeQuery(checkUserSalaryStructAssignedToUserQuery)
    .then((userSalaryStructure) => {
      if (userSalaryStructure && userSalaryStructure.rows.length) {
        next();
      } else {
        response.status(400)
          .json({
            errorType: 'SALARY_STRUCTURE_NOT_ASSIGNED',
            erorrMessage: `User salary structure having id '${request.params.ussid}' is not assigned to user having id '${request.params.uid}'`,
          });
      }
    });
};

exports.isUserAssingedAnySalaryStructure = (request, response, next) => {
  const isUserAssingedAnySalaryStructureQuery = knex('user_salary_structure')
    .select()
    .where('user_id', request.params.uid)
    .limit(1);
  executeQuery(isUserAssingedAnySalaryStructureQuery)
    .then((userSalaryStructure) => {
      if (userSalaryStructure && userSalaryStructure.rows.length) {
        next();
      } else {
        response.status(404)
          .json({
            errorType: 'NO_SALARY_STRUCTURE_ASSIGNED',
            errorMessage: `No salary structure is assinged to user having id '${request.params.uid}'`,
          });
      }
    });
};
