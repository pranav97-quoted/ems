const { executeQuery, knex } = require('../providers/db-connections.js');

exports.createSalaryStruct = (packet) => {
  const createSalaryStructQuery = knex('salary_structures')
    .insert(packet)
    .returning(['id', 'salary_structure_name', 'basic_salary', 'hra', 'tax', 'extra', 'created_at', 'updated_at']);
  return executeQuery(createSalaryStructQuery)
    .then((salaryStructure) => {
      if (salaryStructure && salaryStructure.rows && salaryStructure.rows.length) {
        return salaryStructure.rows[0];
      }
    });
};

exports.getSalaryStructureByName = (salaryStructureName) => {
  const selectSalaryStructureByNameQuery = knex('salary_structures')
    .select('salary_structure_name')
    .where('salary_structure_name', salaryStructureName)
    .limit(1);
  return executeQuery(selectSalaryStructureByNameQuery)
    .then((salaryStructure) => {
      if (salaryStructure && salaryStructure.rows.length) {
        return salaryStructure.rows[0];
      }
    });
};

exports.updateSalaryStructureById = (salaryStructureId, packet) => {
  const updateSalaryStructureByIdQuery = knex('salary_structures')
    .update(packet, ['id'])
    .where('id', salaryStructureId);
  return executeQuery(updateSalaryStructureByIdQuery)
    .then((updatedSalaryStructure) => {
      if (updatedSalaryStructure && updatedSalaryStructure.rows && updatedSalaryStructure.rows.length) {
        return updatedSalaryStructure.rows;
      }
    });
};

exports.deleteSalaryStructureById = (salaryStructureId) => {
  const deleteSalaryStructureByIdQuery = knex('salary_structures')
    .where('id', salaryStructureId)
    .del();
  return executeQuery(deleteSalaryStructureByIdQuery)
    .then((deletedSalaryStructure) => {
      if (!deletedSalaryStructure.rows.length) {
        return `Salary structure having id '${salaryStructureId}' deleted successfully`;
      }
    });
};

exports.getAllSalaryStructures = (itemsPerPage, offset) => {
  const getAllSalaryStructuresQuery = knex('salary_structures')
    .select('basic_salary', 'salary_structure_name', 'hra', 'tax', 'extra', 'created_at', 'updated_at')
    .limit(itemsPerPage)
    .offset(offset);
  return executeQuery(getAllSalaryStructuresQuery)
    .then((salaryStructures) => {
      if (salaryStructures && salaryStructures.rows && salaryStructures.rows.length) {
        return salaryStructures.rows;
      }
    });
};

exports.getAllSalaryStructuresCount = () => {
  const countOfAllSalaryStructuresQuery = knex('salary_structures').count('*');
  return executeQuery(countOfAllSalaryStructuresQuery)
    .then((salaryStructuresCount) => {
      if (salaryStructuresCount && salaryStructuresCount.rows.length) {
        return parseInt(salaryStructuresCount.rows[0].count, 10);
      }
    });
};
