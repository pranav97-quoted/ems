const { executeQuery, knex } = require('../providers/db-connections.js');

exports.createRole = (roleType) => {
  const createRoleQuery = knex('roles')
    .insert({ role_type: roleType })
    .returning(['id', 'role_type']);
  return executeQuery(createRoleQuery)
    .then((role) => role.rows[0]);
};

exports.getRoleByName = (role) => {
  const seleectRoleByNameQuery = knex('roles')
    .select('role_type')
    .limit(1)
    .where('role_type', role);
  return executeQuery(seleectRoleByNameQuery)
    .then((role) => {
      if (role && role.rows) {
        return role.rows;
      }
    });
};

exports.getAllRolesCount = () => {
  const countOfAllRolesQuery = knex('roles').count('*');
  return executeQuery(countOfAllRolesQuery).then((rolesCount) => {
    if (rolesCount && rolesCount.rows) {
      return parseInt(rolesCount.rows[0].count, 10);
    }
  });
};

exports.getRolesPerPage = (itemsPerPage, offset) => {
  const getRolesPerPageQuery = knex('roles')
    .select('id', 'role_type', 'created_at', 'updated_at')
    .limit(itemsPerPage)
    .offset(offset);
  return executeQuery(getRolesPerPageQuery)
    .then((roles) => {
      if (roles && roles.rows) {
        return roles.rows;
      }
    });
};

exports.updateRoleById = (roleType, roleId) => {
  const updateRolesByIdQuery = knex('roles')
    .where('id', roleId)
    .update({
      role_type: roleType,
    },
    ['id', 'role_type', 'created_at', 'updated_at']);
  return executeQuery(updateRolesByIdQuery)
    .then((updatedRole) => {
      if (updatedRole && updatedRole.rows && updatedRole.rows.length) {
        return updatedRole.rows[0];
      }
    });
};

exports.deleteRoleById = (roleId) => {
  const deleteRoleByIdQuery = knex('roles')
    .where({
      id: roleId,
    })
    .del();
  return executeQuery(deleteRoleByIdQuery)
    .then((deletedRole) => {
      if (!deletedRole.rows.length) {
        return `Role having id '${roleId}' deleted successfully`;
      }
    });
};
