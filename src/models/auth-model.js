const { executeQuery, knex } = require('../providers/db-connections.js');

exports.validateUserWithEmail = (email) => {
  const getUserByEmailQuery = knex('users')
    .select('id', 'user_password')
    .where('user_email', email)
    .limit(1);
  return executeQuery(getUserByEmailQuery)
    .then((response) => {
      if (response && response.rows) {
        return response.rows;
      }
    });
};

exports.isUserActive = (email) => {
  const selectUserQuery = knex('users')
    .select('is_active')
    .where('user_email', email)
    .limit(1);
  return executeQuery(selectUserQuery)
    .then((user) => {
      if (user.rows.length && user.rows[0].is_active) {
        return user.rows[0];
      }
    });
};
