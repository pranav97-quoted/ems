const { executeQuery, knex } = require('../providers/db-connections.js');

exports.createUserLeave = (userLeaveArr) => {
  const createUserLeaveQuery = knex('user_leave').insert(userLeaveArr).returning('*');
  return executeQuery(createUserLeaveQuery)
    .then((userLeave) => {
      if (userLeave && userLeave.rows && userLeave.rows.length) {
        return userLeave.rows[0];
      }
    });
};

exports.createHalfDayUserLeave = (userId, leaveId, packet) => {
  const createHalfDayUserLeaveQuery = knex('user_leave').insert({
    user_id: userId,
    leave_id: leaveId,
    leave_date: packet.leave_date,
    leave_day: packet.leave_type,
    approver_id: 1,
    reason: packet.reason,
  })
    .returning('*');
  return executeQuery(createHalfDayUserLeaveQuery)
    .then((halfDayUserLeave) => {
      if (halfDayUserLeave && halfDayUserLeave.rows && halfDayUserLeave.rows.length) {
        return halfDayUserLeave.rows[0];
      }
    });
};

exports.getUserLeaveByLeaveDate = (leaveDate) => {
  const getUserLeaveByLeaveDateQuery = knex('user_leave')
    .select()
    .where('leave_date', leaveDate)
    .limit(1);
  return executeQuery(getUserLeaveByLeaveDateQuery)
    .then((userLeave) => {
      if (userLeave && userLeave.rows && userLeave.rows.length) {
        return userLeave.rows[0];
      }
    });
};

exports.getUserLeaveExistsBetweenStartDateAndEndDate = (startDate, endDate) => {
  const getUserLeaveExistsBetweenStartDateAndEndDateQuery = knex('user_leave')
    .select()
    .where('leave_date', '>=', startDate).andWhere('leave_date', '<', endDate);
  return executeQuery(getUserLeaveExistsBetweenStartDateAndEndDateQuery)
    .then((userLeave) => {
      return userLeave.rows;
    });
};

exports.updateUserLeaveByDate = (leaveDate, userLeaveType, reason) => {
  const updateUserLeaveByIdQuery = knex('user_leave')
    .update({
      leave_day: userLeaveType,
      reason,
    })
    .where('leave_date', leaveDate)
    .returning('id');
  return executeQuery(updateUserLeaveByIdQuery)
    .then((updatedUserLeave) => {
      if (updatedUserLeave && updatedUserLeave.rows && updatedUserLeave.rows.length) {
        return updatedUserLeave.rows[0];
      }
    });
};

exports.updateUserLeave = (userLeaveId, packet) => {
  const updateUserLeaveQuery = knex('user_leave')
    .update(packet)
    .where('id', userLeaveId)
    .returning('id');
  return executeQuery(updateUserLeaveQuery)
    .then((updatedUserLeave) => {
      if (updatedUserLeave && updatedUserLeave.rows && updatedUserLeave.rows.length) {
        return updatedUserLeave.rows[0];
      }
    });
};

exports.deleteUserLeave = (userLeaveId) => {
  const deleteUserLeaveQuery = knex('user_leave')
    .del()
    .where('id', userLeaveId);
  return executeQuery(deleteUserLeaveQuery)
    .then((deletedUserLeave) => {
      if (!deletedUserLeave.rows.length) {
        return `Leave having id '${userLeaveId}' deleted successfully`;
      }
    });
};
