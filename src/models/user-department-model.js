const { executeQuery, knex } = require('../providers/db-connections.js');

exports.createUserDepartment = (userId, departmentId) => {
  const createUserDepartmentQuery = knex('user_department')
    .insert({
      user_id: userId,
      department_id: departmentId,
    })
    .returning('id');
  return executeQuery(createUserDepartmentQuery)
    .then((userDepartment) => {
      if (userDepartment && userDepartment.rows.length) {
        return userDepartment.rows[0];
      }
    });
};

exports.updateUserDepartmentById = (userId, departmentId) => {
  const updateUserDepartmentByIdQuery = knex('user_department')
    .update({
      department_id: departmentId,
    })
    .where('user_id', userId);
  return executeQuery(updateUserDepartmentByIdQuery)
    .then((updatedUserDepartment) => {
      if (updatedUserDepartment && updatedUserDepartment.rows && updatedUserDepartment.rows.length) {
        return updatedUserDepartment.rows[0];
      }
    });
};
