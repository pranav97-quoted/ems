const { executeQuery, knex } = require('../providers/db-connections.js');

exports.createSalary = (userId, userSalaryStructId, salaryDateUTC) => {
  const createSalaryQuery = knex('user_salary')
    .insert({
      user_id: userId,
      user_salary_struct_id: userSalaryStructId,
      salary_date: salaryDateUTC,
    })
    .returning(['id', 'user_id', 'user_salary_struct_id', 'salary_date']);
  return executeQuery(createSalaryQuery)
    .then((userSalary) => {
      if (userSalary && userSalary.rows && userSalary.rows.length) {
        return userSalary.rows[0];
      }
    });
};

exports.getSalaries = (userId, packet, itemsPerPage, offset) => {
  let getSalaryHistoryQuery = knex('user_salary')
    .innerJoin('user_salary_structure', 'user_salary.user_salary_struct_id', '=', 'user_salary_structure.id')
    .innerJoin('salary_structures', 'user_salary_structure.salary_structure_id', '=', 'salary_structures.id')
    .select('user_salary.user_id', 'user_salary.user_salary_struct_id', 'user_salary.salary_date', 'salary_structures.basic_salary')
    .where('user_salary.user_id', userId)
    .limit(itemsPerPage)
    .offset(offset);

  if (packet.startDate && packet.endDate) {
    getSalaryHistoryQuery = getSalaryHistoryQuery.whereBetween('salary_date', [new Date(packet.startDate), new Date(packet.endDate)]);
  } else if (packet.startDate && !packet.endDate) {
    getSalaryHistoryQuery = getSalaryHistoryQuery.andWhere('user_salary.salary_date', '>=', new Date(packet.startDate));
  } else if (packet.endDate && !packet.startDate) {
    getSalaryHistoryQuery = getSalaryHistoryQuery.andWhere('user_salary.salary_date', '<=', new Date(packet.endDate));
  }

  return executeQuery(getSalaryHistoryQuery)
    .then((salaryHistory) => {
      if (salaryHistory && salaryHistory.rows.length) {
        return salaryHistory.rows;
      }
    });
};

exports.getAllSalaryCountByUserId = (userId) => {
  const getAllSalaryCountByUserIdQuery = knex('user_salary').count('*').where('user_id', userId);
  return executeQuery(getAllSalaryCountByUserIdQuery)
    .then((salaryCount) => {
      if (salaryCount && salaryCount.rows) {
        return parseInt(salaryCount.rows[0].count, 10);
      }
    });
};
