const { executeQuery, knex } = require('../providers/db-connections.js');

exports.createCompany = (companyName) => {
  const createCompanyQuery = knex('companies')
    .insert({ company_name: companyName })
    .returning(['id', 'company_name']);
  return executeQuery(createCompanyQuery)
    .then((company) => {
      if (company && company.rows) {
        return company.rows[0];
      }
    });
};

exports.getCompanyByName = (company) => {
  const selectCompanyByNameQuery = knex('companies')
    .select('company_name')
    .limit(1)
    .where('company_name', company);
  return executeQuery(selectCompanyByNameQuery)
    .then((company) => {
      if (company && company.rows) {
        return company.rows;
      }
    });
};

exports.getAllCompaniesCount = () => {
  const countOfAllCompaniesQuery = knex('companies').count('*');
  return executeQuery(countOfAllCompaniesQuery).then((companiesCount) => {
    if (companiesCount && companiesCount.rows) {
      return parseInt(companiesCount.rows[0].count, 10);
    }
  });
};

exports.getCompaniesPerPage = (itemsPerPage, offset) => {
  const getCompaniesPerPageQuery = knex('companies')
    .select('id', 'company_name', 'created_at', 'updated_at')
    .limit(itemsPerPage)
    .offset(offset)
    .orderBy('id');
  return executeQuery(getCompaniesPerPageQuery)
    .then((companies) => {
      if (companies && companies.rows) {
        return companies.rows;
      }
    });
};

exports.updateCompanyById = (companyName, companyId) => {
  const updateCompanyByIdQuery = knex('companies')
    .where('id', companyId)
    .update({
      company_name: companyName,
    },
    ['id', 'company_name', 'created_at', 'updated_at']);
  return executeQuery(updateCompanyByIdQuery)
    .then((updatedCompany) => {
      if (updatedCompany && updatedCompany.rows && updatedCompany.rows.length) {
        return updatedCompany.rows[0];
      }
    });
};

exports.deleteCompanyById = (companyId) => {
  const deleteCompanyByIdQuery = knex('companies')
    .where({
      id: companyId,
    })
    .del();
  return executeQuery(deleteCompanyByIdQuery)
    .then((deletedCompany) => {
      if (!deletedCompany.rows.length) {
        return `Company having id '${companyId}' deleted successfully`;
      }
    });
};
