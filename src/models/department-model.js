const { executeQuery, knex } = require('../providers/db-connections.js');

exports.createDepartment = (departmentName) => {
  const createDepartmentQuery = knex('departments')
    .insert({ department_name: departmentName })
    .returning(['id', 'department_name']);
  return executeQuery(createDepartmentQuery)
    .then((department) => department.rows[0]);
};

exports.getDepartmentByName = (department) => {
  const seleectDepartmentByNameQuery = knex('departments')
    .select('department_name')
    .limit(1)
    .where('department_name', department);
  return executeQuery(seleectDepartmentByNameQuery)
    .then((department) => {
      if (department && department.rows) {
        return department.rows;
      }
    });
};

exports.getAllDepartmentsCount = () => {
  const countOfAllDepartmentsQuery = knex('departments').count('*');
  return executeQuery(countOfAllDepartmentsQuery)
    .then((departmentsCount) => {
      if (departmentsCount && departmentsCount.rows) {
        return parseInt(departmentsCount.rows[0].count, 10);
      }
    });
};

exports.getDepartmentsPerPage = (itemsPerPage, offset) => {
  const getDepartmentsPerPageQuery = knex('departments')
    .select('id', 'department_name', 'created_at', 'updated_at')
    .limit(itemsPerPage)
    .offset(offset);
  return executeQuery(getDepartmentsPerPageQuery)
    .then((departments) => {
      if (departments && departments.rows) {
        return departments.rows;
      }
    });
};

exports.updateDepartmentById = (departmentName, departmentId) => {
  const updateDepartmentByIdQuery = knex('departments')
    .where('id', departmentId)
    .update({
      department_name: departmentName,
    },
    ['id', 'department_name', 'created_at', 'updated_at']);
  return executeQuery(updateDepartmentByIdQuery)
    .then((updatedDepartment) => {
      if (updatedDepartment && updatedDepartment.rows && updatedDepartment.rows.length) {
        return updatedDepartment.rows[0];
      }
    });
};

exports.deleteDepartmentById = (departmentId) => {
  const deleteDepartmentByIdQuery = knex('departments')
    .where({
      id: departmentId,
    })
    .del();
  return executeQuery(deleteDepartmentByIdQuery)
    .then((deletedDepartment) => {
      if (!deletedDepartment.rows.length) {
        return `Department having id '${departmentId}' deleted successfully`;
      }
    });
};
