const express = require('express');

const authController = require('../../controllers/auth-controller.js');
const companyController = require('../../controllers/company-controller.js');
const departmentController = require('../../controllers/department-controller.js');
const roleController = require('../../controllers/role-controller.js');
const userController = require('../../controllers/user-controller.js');
const salaryStructController = require('../../controllers/salary-struct-controller.js');
const userSalaryStructController = require('../../controllers/user-salary-struct-controller.js');
const userSalaryController = require('../../controllers/salary-controller.js');
const leaveController = require('../../controllers/leave-controller.js');
const userLeaveController = require('../../controllers/user-leave-controller.js');
const documentUploadController = require('../../controllers/document-upload-controller.js');

const { checkAdminAuth, isUserAuthorized } = require('../../middlewares/auth-middleware.js');

const { checkCompanyExistsById } = require('../../middlewares/company-middleware.js');

const { checkDepartmentExistsById } = require('../../middlewares/department-middleware.js');

const { checkRoleExistsById } = require('../../middlewares/role-middleware.js');

const { validateAuthLogin } = require('../../validators/auth-validator.js');

const {
  validateCreateUser, validateConfirmUser, validateUpdateUser, validateDeleteUser,
} = require('../../validators/user-validator.js');

const {
  checkUserExistsByEmailAndIsActive, isEmailAndConfirmationCodeValid, checkUserExistsById, isUserAdmin,
} = require('../../middlewares/user-middleware.js');

const { checkSalaryStructExistsById, isSalaryStructAssignedToUser } = require('../../middlewares/salary-struct-middleware.js');

const {
  validateDataChunk,
} = require('../../validators/validate-data-chunk.js');

const { validateCreateCompany, validateUpdateCompany, validateDeleteCompany } = require('../../validators/company-validator.js');

const { validateCreateDepartment, validateUpdateDepartment, validateDeleteDepartment } = require('../../validators/department-validator.js');

const { validateCreateRole, validateUpdateRole, validateDeleteRole } = require('../../validators/role-validator.js');

const { validateCreateSalaryStruct, validateUpdateSalaryStruct, validateDeleteSalaryStruct } = require('../../validators/salary-struct-validator.js');

const { validateUserSalaryStructAssign, validateDeleteUserSalaryStruct } = require('../../validators/user-salary-struct-validator.js');

const {
  isAnySalaryStructAssignedToUser, checkUserSalaryStructExistsById, checkUserSalaryStructAssignedToUser, isUserAssingedAnySalaryStructure,
} = require('../../middlewares/user-salary-struct-middleware.js');

const { validateCreateSalary, validateGetSalaryHistory } = require('../../validators/salary-validator.js');

const { validateCreateLeave, validateDeleteLeave, validateUpdateLeave } = require('../../validators/leave-validator.js');

const { checkLeaveExistsById, isLeaveAssignedToAnyUser } = require('../../middlewares/leave-middleware.js');

const { validateUserLeaveAssign, validateUpdateUserLeave, validateDeleteUserLeave } = require('../../validators/user-leave-validator.js');

const { isAnyLeaveAssignedToUser, isUserLeaveExistsById, isLeaveInPendingState } = require('../../middlewares/user-leave-middleware.js');

const { fileUpload, checkDocumentExistsById } = require('../../middlewares/document-upload-middleware.js');

const router = express.Router();

router.use((req, res, next) => {
  console.log('=================== Endpoint: ', req.url, '=================================');
  next();
});

router.post('/users', [validateCreateUser, isUserAuthorized, checkAdminAuth, checkRoleExistsById, checkDepartmentExistsById, checkCompanyExistsById], userController.createUser);

router.post('/companies', [validateCreateCompany, isUserAuthorized, checkAdminAuth], companyController.createCompany);

router.post('/employment-types', (req, res) => {
  res.status(201).send();
});

router.post('/roles', [validateCreateRole, isUserAuthorized, checkAdminAuth], roleController.createRole);

router.post('/departments', [validateCreateDepartment, isUserAuthorized, checkAdminAuth], departmentController.createDepartment);

router.post('/leaves', [validateCreateLeave, isUserAuthorized, checkAdminAuth], leaveController.createLeave);

router.post('/salary-structures', [validateCreateSalaryStruct, isUserAuthorized, checkAdminAuth], salaryStructController.createSalaryStruct);

router.post('/users/:uid/documents', [checkUserExistsById, isUserAuthorized, checkAdminAuth, fileUpload], documentUploadController.uploadDocument);

router.get('/users', (req, res) => {
  res.status(200).send();
});
router.get('/users/confirmation', userController.tempConfirmUser);

router.patch('/users/confirmation', [validateConfirmUser, checkUserExistsByEmailAndIsActive, isEmailAndConfirmationCodeValid], userController.confirmUser);

router.get('/users/cu', [isUserAuthorized, checkAdminAuth], userController.getCurrentLoggedInUser);

router.get('/companies', [isUserAuthorized, checkAdminAuth, validateDataChunk], companyController.getCompanies);

router.get('/users/:uid/documents', [checkUserExistsById, isUserAuthorized, checkAdminAuth, validateDataChunk], documentUploadController.getDocuments);

router.get('/users/:uid/documents/:doid', [checkUserExistsById, checkDocumentExistsById, isUserAuthorized, checkAdminAuth], documentUploadController.downloadDocument);

router.delete('/users/:uid/documents/:doid', [checkUserExistsById, checkDocumentExistsById, isUserAuthorized, checkAdminAuth], documentUploadController.deleteDocument);

router.get('/companies/:cid', (req, res) => {
  res.status(200).send();
});

router.get('/employment-types', (req, res) => {
  res.status(200).send();
});

router.get('/employment-types/:etid', (req, res) => {
  res.status(200).send();
});

router.get('/roles', [isUserAuthorized, checkAdminAuth, validateDataChunk], roleController.getRoles);

router.get('/roles/:rid', (req, res) => {
  res.status(200).send();
});

router.get('/departments', [isUserAuthorized, checkAdminAuth, validateDataChunk], departmentController.getDepartments);

router.get('/departments/:did', (req, res) => {
  res.status(200).send();
});

router.get('/leaves', [isUserAuthorized, checkAdminAuth, validateDataChunk], leaveController.getLeaves);

router.get('/leaves/:lid', (req, res) => {
  res.status(200).send();
});

router.get('/salary-structures', [isUserAuthorized, checkAdminAuth, validateDataChunk], salaryStructController.getAllSalaryStructures);

router.get('/salary-structures/:sid', (req, res) => {
  res.status(200).send();
});

router.patch('/users/:uid', [checkUserExistsById, validateUpdateUser, isUserAuthorized, checkAdminAuth, checkRoleExistsById, checkDepartmentExistsById, checkCompanyExistsById], userController.updateUserById);

router.delete('/users/:uid', [validateDeleteUser, isUserAuthorized, checkAdminAuth, checkUserExistsById, isUserAdmin], userController.deleteUserById);

router.patch('/companies/:cid', [validateUpdateCompany, isUserAuthorized, checkAdminAuth, checkCompanyExistsById], companyController.updateCompanyById);

router.delete('/companies/:cid', [validateDeleteCompany, isUserAuthorized, checkAdminAuth, checkCompanyExistsById], companyController.deleteCompanyById);

router.patch('/employment-types/:eid', (req, res) => {
  res.status(200).send();
});

router.delete('/employment-types/:eid', (req, res) => {
  res.status(204).send();
});

router.patch('/roles/:rid', [validateUpdateRole, isUserAuthorized, checkAdminAuth, checkRoleExistsById], roleController.updateRoleById);

router.delete('/roles/:rid', [validateDeleteRole, isUserAuthorized, checkAdminAuth, checkRoleExistsById], roleController.deleteRoleById);

router.patch('/departments/:did', [validateUpdateDepartment, isUserAuthorized, checkAdminAuth, checkDepartmentExistsById], departmentController.updateDepartmentById);

router.delete('/departments/:did', [validateDeleteDepartment, isUserAuthorized, checkAdminAuth, checkDepartmentExistsById], departmentController.deleteDepartmentById);

router.patch('/leaves/:lid', [validateUpdateLeave, isUserAuthorized, checkAdminAuth, checkLeaveExistsById], leaveController.updateLeaveById);

router.delete('/leaves/:lid', [validateDeleteLeave, isUserAuthorized, checkAdminAuth, checkLeaveExistsById, isLeaveAssignedToAnyUser], leaveController.deleteLeaveById);

router.patch('/salary-structures/:sid', [validateUpdateSalaryStruct, isUserAuthorized, checkAdminAuth, checkSalaryStructExistsById], salaryStructController.updateSalaryStructureById);

router.delete('/salary-structures/:sid', [validateDeleteSalaryStruct, isUserAuthorized, checkAdminAuth, checkSalaryStructExistsById, isSalaryStructAssignedToUser], salaryStructController.deleteSalaryStructureById);

router.post('/users/:uid/leaves', [validateUserLeaveAssign, checkUserExistsById, checkLeaveExistsById, isUserAuthorized, checkAdminAuth, isAnyLeaveAssignedToUser], userLeaveController.createUserLeave);

router.patch('/users/:uid/leaves/:ulid', [validateUpdateUserLeave, checkUserExistsById, checkLeaveExistsById, isUserAuthorized, checkAdminAuth, isUserLeaveExistsById, isLeaveInPendingState], userLeaveController.updateUserLeave);

router.get('/users/:uid/leaves/:lid', (req, res) => {
  res.status(200).send();
});

router.delete('/users/:uid/leaves/:ulid', [validateDeleteUserLeave, checkUserExistsById, isUserAuthorized, checkAdminAuth, isUserLeaveExistsById, isLeaveInPendingState], userLeaveController.deleteUserLeave);

router.put('/users/:uid/departments/:did', (req, res) => {
  res.status(200).send();
});

router.delete('/users/:uid/departments/:did', (req, res) => {
  res.status(204).send();
});

router.get('/users/:uid/departments', (req, res) => {
  res.status(200).send();
});

router.get('/users/:uid/departments/:did', (req, res) => {
  res.status(200).send();
});

router.put('/users/:uid/roles/:rid', (req, res) => {
  res.status(200).send();
});

router.delete('/users/:uid/roles/:rid', (req, res) => {
  res.status(204).send();
});

router.get('/users/:uid/roles', (req, res) => {
  res.status(200).send();
});

router.get('/users/:uid/roles/:rid', (req, res) => {
  res.status(200).send();
});

router.put('/users/:uid/approvers/:aid', (req, res) => {
  res.status(200).send();
});

router.delete('/users/:uid/approvers/:aid', (req, res) => {
  res.status(200).send();
});

router.get('/users/:uid/approvers', (req, res) => {
  res.status(200).send();
});

router.get('/users/:uid/approvers/:aid', (req, res) => {
  res.status(200).send();
});

router.put('/users/:uid/salary-structures/:ssid', [validateUserSalaryStructAssign, checkUserExistsById, checkSalaryStructExistsById, isUserAuthorized, checkAdminAuth, isAnySalaryStructAssignedToUser], userSalaryStructController.assignSalaryStructToUser);

router.delete('/users/:uid/salary-structures/:ussid', [validateDeleteUserSalaryStruct, checkUserExistsById, checkUserSalaryStructExistsById, checkUserSalaryStructAssignedToUser, isUserAuthorized, checkAdminAuth], userSalaryStructController.deleteSalaryStructForUserById);

router.get('/users/:uid/salary-structures', [isUserAuthorized, checkAdminAuth, checkUserExistsById, isUserAssingedAnySalaryStructure, validateDataChunk], userSalaryStructController.getUserSalaryStructures);

router.get('/users/:uid/salary-structures/:sid', (req, res) => {
  res.status(200).send();
});

router.post('/users/:uid/salaries', [validateCreateSalary, checkUserExistsById, isUserAuthorized, checkAdminAuth, isUserAssingedAnySalaryStructure], userSalaryController.createSalary);

router.get('/users/:uid/salaries', [validateGetSalaryHistory, validateDataChunk, checkUserExistsById, isUserAuthorized, checkAdminAuth], userSalaryController.getSalaries);

router.get('/users/:uid/salaries/:sid', (req, res) => {
  res.status(200).send();
});

router.put('/users/:uid/salaries/:sid', (req, res) => {
  res.status(200).send();
});

router.post('/auth', validateAuthLogin, authController.auth);

router.get('/auth/status', authController.authStatus);

router.get('/auth/logout', authController.authLogout);

module.exports = router;
