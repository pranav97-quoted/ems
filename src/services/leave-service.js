const {
  getLeaves, getLeavesCount, createLeave, getLeaveByLeaveType, deleteLeaveById, updateLeaveById,
} = require('../models/leave-model.js');

exports.createLeave = (packet) => getLeaveByLeaveType(packet.leave_type)
  .then((leave) => {
    if (leave) {
      const err = new Error();
      err.errorType = 'LEAVE_ALREADY_CREATED';
      err.errorMessage = `Leave with type '${packet.leave_type}' already exists`;
      err.statusCode = 400;
      throw err;
    } else {
      return createLeave(packet)
        .then((leave) => {
          if (leave) {
            return leave;
          }
        });
    }
  });

exports.getLeaves = (itemsPerPage, offset) => Promise.all([getLeaves(itemsPerPage, offset), getLeavesCount()])
  .then((results) => results);

exports.deleteLeaveById = (leaveId) => deleteLeaveById(leaveId).then((deletedLeave) => deletedLeave);

exports.updateLeaveById = (leaveType, leaveId) => getLeaveByLeaveType(leaveType)
  .then((leave) => {
    if (leave) {
      const err = new Error();
      err.errorType = 'LEAVE_ALREADY_CREATED';
      err.errorMessage = `Leave with type '${leaveType}' already exists`;
      err.statusCode = 400;
      throw err;
    } else {
      return updateLeaveById(leaveType, leaveId)
        .then((updatedLeave) => {
          if (updatedLeave) {
            return updatedLeave;
          }
        });
    }
  });
