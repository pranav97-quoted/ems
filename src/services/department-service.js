const {
  createDepartment, getDepartmentByName, getDepartmentsPerPage, getAllDepartmentsCount, updateDepartmentById, deleteDepartmentById,
} = require('../models/department-model.js');

exports.createDepartment = (packet) => getDepartmentByName(packet.department_name)
  .then((department) => {
    if (department && department.length) {
      const err = new Error();
      err.errorType = 'DEPARTMENT_ALREADY_EXISTS';
      err.errorMessage = `Department with name '${department[0].department_name}' already exists`;
      err.statusCode = 400;
      throw err;
    } else {
      return createDepartment(packet.department_name)
        .then((department) => {
          if (department) {
            return department;
          }
        });
    }
  });

exports.getDepartments = (itemsPerPage, offset) => Promise.all([getDepartmentsPerPage(itemsPerPage, offset), getAllDepartmentsCount()]).then((results) => results);

exports.updateDepartmentById = (departmentName, departmentId) => getDepartmentByName(departmentName)
  .then((department) => {
    if (department && department.length) {
      const err = new Error();
      err.errorType = 'DEPARTMENT_ALREADY_EXISTS';
      err.errorMessage = `Cannot update department as department with name '${department[0].department_name}' already exists`;
      throw err;
    } else {
      return updateDepartmentById(departmentName, departmentId).then((updatedDepartment) => {
        if (updatedDepartment) {
          return updatedDepartment;
        }
      });
    }
  });

exports.deleteDepartmentById = (departmentId) => deleteDepartmentById(departmentId)
  .then((deletedDepartment) => {
    if (deletedDepartment) {
      return deletedDepartment;
    }
  });
