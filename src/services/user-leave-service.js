const moment = require('moment');
const promise = require('bluebird');
const {
  createUserLeave, getUserLeaveByLeaveDate, updateUserLeaveByDate: updateUserLeaveByDate, createHalfDayUserLeave, updateUserLeave, deleteUserLeave, getUserLeaveExistsBetweenStartDateAndEndDate
} = require('../models/user-leave-model.js');

exports.createUserLeave = (userId, packet) => {
  if (packet.half_day_date && moment(packet.start_date).utc().isSame(moment(packet.half_day_date).utc()) && moment(packet.end_date).utc().diff(moment(packet.start_date).utc(), 'days') === 1) {
    const halfDayDate = moment(packet.half_day_date).utc().format('YYYY-MM-DD');
    return getUserLeaveByLeaveDate(halfDayDate)
      .then((userLeave) => {
        if (userLeave && userLeave.leave_day === 'half_day') {
          const leaveDate = moment(packet.half_day_date).utc().format('YYYY-MM-DD');
          return updateUserLeaveByDate(leaveDate, 'full_day', packet.reason || userLeave.reason)
            .then((updatedUserLeave) => {
              if (updatedUserLeave) {
                return 'Leave updated successfully from half day to full day';
              }
            });
        }
        const finalPacket = {};
        finalPacket.leave_date = moment(packet.half_day_date).utc().format('YYYY-MM-DD');
        finalPacket.leave_type = 'half_day';
        finalPacket.reason = packet.reason || '';
        return createHalfDayUserLeave(userId, packet.leave_id, finalPacket)
          .then((halfDayLeave) => {
            if (halfDayLeave) {
              return 'Leave created successfully';
            }
          });
      });
  }
  const startDate = moment(packet.start_date).utc();
  const endDate = moment(packet.end_date).utc();
  const leaveCount = endDate.diff(startDate, 'days');
  const dateIterator = Array.from({ length: leaveCount }, (itm, idx) => idx);
  const dateArr = [];
  const temDate = startDate;
  for (let i = 0; i < dateIterator.length; i++) {
    dateArr.push(temDate.format('YYYY-MM-DD'));
    temDate.add(1, 'days');
  }
  const limit = moment(packet.end_date).utc().diff(moment(packet.start_date).utc(), 'days');
  return getUserLeaveExistsBetweenStartDateAndEndDate(moment(packet.start_date).utc(), moment(packet.end_date).utc())
    .then((userLeaves) => {
      userLeaves = userLeaves && userLeaves.map((userLeave) => {
        return { ...userLeave, leave_date: moment(userLeave.leave_date).format('YYYY-MM-DD') }
      });
      if (userLeaves && userLeaves.length) {
        let createUserLeaveArr = [];
        createUserLeaveArr = dateArr.filter((dateElement) => {
          return !userLeaves.find((userLeave) => {
            return moment(userLeave.leave_date).format('YYYY-MM-DD') === dateElement;
          });
        })
          .map((finalDateElement) => {
            const finalPacket = {};
            finalPacket.leave_date = moment(finalDateElement).format('YYYY-MM-DD');
            if (packet.half_day_date && moment(finalDateElement).isSame(moment(packet.half_day_date).utc())) {
              finalPacket.leave_day = 'half_day';
            } else {
              finalPacket.leave_day = 'full_day';
            }
            if (packet.reason) {
              finalPacket.reason = packet.reason;
            }
            finalPacket.user_id = userId;
            finalPacket.leave_id = packet.leave_id;
            finalPacket.approver_id = 1;
            return finalPacket;
          });
        if (createUserLeaveArr.length) {
          return promise.mapSeries(userLeaves, (userLeave) => {
            return updateUserLeaveByDate(userLeave.leave_date, 'full_day', userLeave.reason)
              .then((updatedUserLeaves) => updatedUserLeaves);
          }).then((updatedUserLeaves) => {
            if (updatedUserLeaves) {
              return createUserLeave(createUserLeaveArr)
                .then((createdUserLeave) => {
                  if (createdUserLeave) {
                    return 'Leaves created successfully';
                  }
                });
            }
          });
        }

        return promise.mapSeries(userLeaves, (userLeave) => {
          return updateUserLeaveByDate(userLeave.leave_date, 'full_day', userLeave.reason)
        })
          .then(() => {
            return 'Leaves created successfully';
          });
      }
      const leaveCountInDays = moment(packet.end_date).utc().diff(moment(packet.start_date).utc(), 'days');
      const leaveCountIterator = Array.from({ length: leaveCountInDays }, (itm, idx) => idx);
      const userLeaveArr = leaveCountIterator
        .map(() => {
          const finalPacket = {};
          finalPacket.leave_date = moment(packet.start_date).utc().format('YYYY-MM-DD');
          if (packet.half_day_date && moment(packet.start_date).utc().isSame(moment(packet.half_day_date).utc())) {
            finalPacket.leave_day = 'half_day';
          } else {
            finalPacket.leave_day = 'full_day';
          }
          if (packet.reason) {
            finalPacket.reason = packet.reason;
          }
          finalPacket.user_id = userId;
          finalPacket.leave_id = packet.leave_id;
          finalPacket.approver_id = 1;
          packet.start_date = moment(packet.start_date).utc().add(1, 'days');
          return finalPacket;
        });
      return createUserLeave(userLeaveArr)
        .then((userLeave) => {
          if (userLeave) {
            return 'Leaves created successfully';
          }
        });
    });
};

exports.updateUserLeave = (userLeaveId, packet) => updateUserLeave(userLeaveId, packet)
  .then((updatedUserLeave) => {
    if (updatedUserLeave) {
      return `Leave having id '${userLeaveId}' updated successfully`;
    }
  });

exports.deleteUserLeave = (userLeaveId) => deleteUserLeave(userLeaveId)
  .then((deletedUserLeave) => deletedUserLeave);
