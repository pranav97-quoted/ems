const fs = require('fs');

const {
  getDocuments, getDocumentsCount, uploadDocument, downloadDocument, deleteDocument,
} = require('../models/document-upload-model.js');

exports.getDocuments = (itemsPerPage, offset, userId) => Promise.all([getDocuments(itemsPerPage, offset, userId), getDocumentsCount(userId)]).then((results) => results);

exports.uploadDocument = (userId, packet) => {
  if (packet) {
    return uploadDocument(userId, packet.filename).then((result) => result);
  }
  const err = new Error();
  err.errorType = 'FILE_NOT_FOUND';
  err.errorMessage = 'No file(s) have been uploaded';
  err.statusCode = 404;
  throw err;
};

exports.downloadDocument = (documentId, userId) => downloadDocument(documentId, userId)
  .then((document) => document);

exports.deleteDocument = (documentId, userId) => downloadDocument(documentId, userId)
  .then((document) => deleteDocument(documentId, userId)
    .then((deletedDocument) => new Promise((resolve, reject) => {
      fs.unlink(`./static/users/${userId}/${document.file_name}`, (err) => {
        if (err) {
          err.errorType = 'ERROR_DELETING_FILE';
          err.errorMessage = 'There is error deleting the document. Please try again later';
          err.statusCode = 400;
          reject(err);
        } else {
          resolve(deletedDocument);
        }
      });
    }).then((result) => result)
      .catch((err) => {
        throw err;
      })));
