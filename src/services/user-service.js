const { sendEmailConfirmation } = require('../lib/email-confirmation.js');
const {
  createUser, getUserByEmail, confirmUser, deleteUserById, updateUserById, getCurrentLoggedInUser
} = require('../models/user-model.js');
const { createUserRole, updateUserRoleById } = require('../models/user-role-model.js');
const { createUserDepartment, updateUserDepartmentById } = require('../models/user-department-model.js');

exports.createUser = (packet) => getUserByEmail(packet.user_email)
  .then((user) => {
    if (user && user.length) {
      const err = new Error();
      err.errorType = 'USER_ALREADY_EXISTS';
      err.errorMessage = `User having email '${user[0].user_email}' already exists`;
      err.statusCode = 400;
      throw err;
    } else {
      const roleId = packet.role_id;
      let departmentId;
      if (packet.department_id) {
        departmentId = packet.department_id;
      }
      return createUser(packet)
        .then((user) => {
          if (user) {
            const promiseArr = [];
            promiseArr.push(createUserRole(user.id, roleId));
            if (departmentId) {
              promiseArr.push(createUserDepartment(user.id, departmentId));
            }
            return Promise.all(promiseArr)
              .then((results) => {
                if (results) {
                  const emailHeaders = {
                    to: packet.user_email,
                    from: 'malik@quotedinfotech.com',
                    subject: 'Confirm Your Account',
                  };

                  const emailBody = {
                    html: `<p>Dear ${packet.first_name}</p>
                    <p>A new account is created for you</p>
                    <p>Your login id is: <a href=#>${packet.user_email}</a></p>
                    <p>Click on the link below to complete your registration</p>
                    <a href=${process.env.API_HOST}/users/confirmation?email=${packet.user_email}&confirmationCode=${user.confirmation_code}>${user.confirmation_code}</a>`,
                  };

                  return sendEmailConfirmation(emailHeaders, emailBody)
                    .then((mailNotification) => mailNotification);
                }
              });
          }
        });
    }
  });

exports.updateUserById = (userId, packet) => {
  let departmentId; let
    roleId;
  if (packet.department_id) {
    departmentId = packet.department_id;
  }
  if (packet.role_id) {
    roleId = packet.role_id;
  }
  const promiseArr = [];
  promiseArr.push(updateUserById(userId, packet));
  if (departmentId) {
    promiseArr.push(updateUserDepartmentById(userId, departmentId));
  }
  if (roleId) {
    promiseArr.push(updateUserRoleById(userId, roleId));
  }
  return Promise.all(promiseArr)
    .then((results) => {
      if (results.length) {
        return 'User details updated successfully';
      }
    });
};

exports.confirmUser = (email, password) => confirmUser(email, password)
  .then((updatedUserStatus) => updatedUserStatus);

exports.deleteUserById = (userId) => deleteUserById(userId)
  .then((deletedUser) => {
    if (deletedUser) {
      return deletedUser;
    }
  });

exports.getCurrentLoggedInUser = (loggedInUserId) => getCurrentLoggedInUser(loggedInUserId).then((currentLoggedInUser) => currentLoggedInUser);
