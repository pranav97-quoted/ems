const {
  createRole, getRoleByName, getRolesPerPage, getAllRolesCount, updateRoleById, deleteRoleById,
} = require('../models/role-model.js');

exports.createRole = (packet) => getRoleByName(packet.role_type)
  .then((role) => {
    if (role && role.length) {
      const err = new Error();
      err.errorType = 'ROLE_ALREADY_EXISTS';
      err.errorMessage = `Role with name '${role[0].role_type}' already exists`;
      err.statusCode = 400;
      throw err;
    } else {
      return createRole(packet.role_type)
        .then((role) => {
          if (role) {
            return role;
          }
        });
    }
  });

exports.getRoles = (itemsPerPage, offset) => Promise.all([getRolesPerPage(itemsPerPage, offset), getAllRolesCount()]).then((results) => results);

exports.updateRoleById = (roleType, roleId) => getRoleByName(roleType)
  .then((role) => {
    if (role && role.length) {
      const err = new Error();
      err.errorType = 'ROLE_ALREADY_EXISTS';
      err.errorMessage = `Cannot update role as role with name '${role[0].role_type}' already exists`;
      throw err;
    } else {
      return updateRoleById(roleType, roleId).then((updatedRole) => {
        if (updatedRole) {
          return updatedRole;
        }
      });
    }
  });

exports.deleteRoleById = (roleId) => deleteRoleById(roleId)
  .then((deletedRole) => {
    if (deletedRole) {
      return deletedRole;
    }
  });
