const {
  assignSalaryStructToUser, deleteSalaryStructForUserById, getAllUserSalaryStructCount, getUserSalaryStructures,
} = require('../models/user-salary-struct-model.js');

exports.assignSalaryStructToUser = (userId, salaryStructId, packet) => assignSalaryStructToUser(userId, salaryStructId, packet)
  .then((userSalaryStructure) => {
    if (userSalaryStructure) {
      return userSalaryStructure;
    }
  });

exports.deleteSalaryStructForUserById = (userSalaryStructId) => deleteSalaryStructForUserById(userSalaryStructId)
  .then((deletedUserSalaryStruct) => {
    if (deletedUserSalaryStruct) {
      return deletedUserSalaryStruct;
    }
  });

exports.getUserSalaryStructures = (itemsPerPage, offset) => Promise.all([getUserSalaryStructures(itemsPerPage, offset), getAllUserSalaryStructCount()]).then((results) => results);
