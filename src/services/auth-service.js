const bcrypt = require('bcryptjs');
const { validateUserWithEmail, isUserActive } = require('../models/auth-model.js');

exports.authService = (packet) => validateUserWithEmail(packet.email)
  .then((users) => {
    if (users && users.length) {
      return isUserActive(packet.email).then((user) => {
        if (user) {
          return bcrypt.compare(packet.password, users[0].user_password)
            .then((result) => {
              if (result) {
                return users;
              }
              const err = new Error();
              err.errorType = 'INVALID_USER_PASSWORD';
              err.errorMessage = 'Bad Credentials';
              err.statusCode = 400;
              throw err;
            });
        }
        const err = new Error();
        err.errorType = 'UNAUTHORIZED_ACCESS';
        err.errorMessage = 'Please confirm your email first';
        err.statusCode = 401;
        throw err;
      });
    }
    const err = new Error();
    err.errorType = 'INVALID_USER_EMAIL';
    err.errorMessage = 'Bad Credentials';
    err.statusCode = 400;
    throw err;
  });
