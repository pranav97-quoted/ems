const Joi = require('@hapi/joi');

exports.validateCreateRole = (request, response, next) => {
  const packet = {
    ...request.body,
  };

  const schema = Joi.object({
    role_type: Joi.string().min(3).required(),
  });

  schema.validateAsync(packet)
    .then((result) => {
      if (result) {
        next();
      }
    })
    .catch((err) => {
      response.status(400)
        .json({
          errorType: 'VALIDATION_ERROR',
          errorMessage: err.details[0].message,
        });
    });
};

exports.validateUpdateRole = (request, response, next) => {
  const packet = {
    ...request.params,
    ...request.body,
  };

  const schema = Joi.object({
    rid: Joi.number().integer().min(1),
    role_type: Joi.string().min(3).required(),
  });

  schema.validateAsync(packet)
    .then((result) => {
      if (result) {
        next();
      }
    })
    .catch((err) => {
      response.status(400)
        .json({
          errorType: 'VALIDATION_ERROR',
          errorMessage: err.details[0].message,
        });
    });
};

exports.validateDeleteRole = (request, response, next) => {
  const packet = {
    ...request.params,
  };

  const schema = Joi.object({
    rid: Joi.number().integer().min(1),
  });

  schema.validateAsync(packet)
    .then((result) => {
      if (result) {
        next();
      }
    })
    .catch((err) => {
      response.status(400)
        .json({
          errorType: 'VALIDATION_ERROR',
          errorMessage: err.details[0].message,
        });
    });
};
