const Joi = require('@hapi/joi');
const moment = require('moment');

exports.validateUserLeaveAssign = (request, response, next) => {
  const packet = {
    ...request.params,
    ...request.body,
  };

  const schema = Joi.object({
    uid: Joi.number().integer().min(1),
    leave_id: Joi.number().integer().min(1).required(),
    start_date: Joi.date().required(),
    end_date: Joi.date().greater(Joi.ref('start_date')).required(),
    reason: Joi.string().min(10),
    leave_type: Joi.string().valid('half_day', 'full_day').required(),
    half_day_date: Joi.when('leave_type', {
      is: Joi.string().valid('half_day').required(),
      then: Joi.date().greater(moment(request.body.start_date).subtract(1, 'days').format('YYYY-MM-DD')).less(moment(request.body.end_date).add(1, 'days').format('YYYY-MM-DD')).required(),
      otherwise: Joi.forbidden(),
    }),
  });

  schema.validateAsync(packet)
    .then((result) => {
      if (result) {
        next();
      }
    })
    .catch((err) => {
      response.status(400)
        .json({
          errorType: 'VALIDATION_ERROR',
          errorMessage: err.details[0].message,
        });
    });
};

exports.validateUpdateUserLeave = (request, response, next) => {
  const packet = {
    ...request.params,
    ...request.body,
  };

  const schema = Joi.object({
    uid: Joi.number().integer().min(1),
    ulid: Joi.number().integer().min(1),
    leave_id: Joi.number().integer().min(1),
    reason: Joi.string().min(10),
    leave_day: Joi.string().valid('half_day', 'full_day'),
  });

  schema.validateAsync(packet)
    .then((result) => {
      if (result) {
        next();
      }
    })
    .catch((err) => {
      response.status(400)
        .json({
          errorType: 'VALIDATION_ERROR',
          errorMessage: err.details[0].message,
        });
    });
};

exports.validateDeleteUserLeave = (request, response, next) => {
  const packet = {
    ...request.params,
  };

  const schema = Joi.object({
    uid: Joi.number().integer().min(1),
    ulid: Joi.number().integer().min(1),
  });

  schema.validateAsync(packet)
    .then((result) => {
      if (result) {
        next();
      }
    })
    .catch((err) => {
      response.status(400)
        .json({
          errorType: 'VALIDATION_ERROR',
          errorMessage: err.details[0].message,
        });
    });
};
