const Joi = require('@hapi/joi');

exports.validateDataChunk = (request, response, next) => {
  const schema = Joi.object({
    itemsPerPage: Joi.number().integer().min(1).max(100),
    offset: Joi.number().min(0),
    startDate: Joi.date(),
    endDate: Joi.when('startDate', {
      then: Joi.date().greater(Joi.ref('startDate')),
      otherwise: Joi.date(),
    }),
  });

  schema.validateAsync(request.query)
    .then((result) => {
      if (result) {
        next();
      }
    }).catch((err) => {
      response.status(400)
        .json({
          errorType: 'VALIDATION_ERROR',
          errorMessage: err.details[0].message,
        });
    });
};
