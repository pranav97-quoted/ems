const Joi = require('@hapi/joi');

exports.validateCreateSalary = (request, response, next) => {
  const packet = {
    ...request.params,
    ...request.body,
  };

  const schema = Joi.object({
    uid: Joi.number().integer().min(1),
    salary_date: Joi.date().required(),
  });

  schema.validateAsync(packet)
    .then((result) => {
      if (result) {
        next();
      }
    })
    .catch((err) => {
      response.status(400)
        .json({
          errorType: 'VALIDATION_ERROR',
          errorMessage: err.details[0].message,
        });
    });
};

exports.validateGetSalaryHistory = (request, response, next) => {
  const packet = {
    ...request.parms,
  };

  const schema = Joi.object({
    uid: Joi.number().integer().min(1),
  });

  schema.validateAsync(packet).then(packet)
    .then((result) => {
      if (result) {
        next();
      }
    })
    .catch((err) => {
      response.status(400)
        .json({
          errorType: 'VALIDATION_ERROR',
          errorMessage: err.details[0].message,
        });
    });
};
