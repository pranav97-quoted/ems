const Joi = require('@hapi/joi');

exports.validateUserSalaryStructAssign = (request, response, next) => {
  const packet = {
    ...request.params,
    ...request.body,
  };

  const schema = Joi.object({
    uid: Joi.number().integer().min(1),
    ssid: Joi.number().integer().min(1),
    start_time: Joi.date().required(),
    end_time: Joi.date().required(),
  });

  schema.validateAsync(packet)
    .then((result) => {
      if (result) {
        next();
      }
    })
    .catch((err) => {
      response.status(400)
        .json({
          errorType: 'VALIDATION_ERROR',
          errorMessage: err.details[0].message,
        });
    });
};

exports.validateDeleteUserSalaryStruct = (request, response, next) => {
  const packet = {
    ...request.params,
  };

  const schema = Joi.object({
    uid: Joi.number().integer().min(1),
    ussid: Joi.number().integer().min(1),
  });

  schema.validateAsync(packet).then((result) => {
    if (result) {
      next();
    }
  })
    .catch((err) => {
      response.status(400)
        .json({
          errorType: 'VALIDATION_ERROR',
          errorMessage: err.details[0].message,

        });
    });
};
