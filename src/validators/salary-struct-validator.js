const Joi = require('@hapi/joi');

exports.validateCreateSalaryStruct = (request, response, next) => {
  const packet = {
    ...request.body,
  };

  const schema = Joi.object({
    salary_structure_name: Joi.string().min(3).required(),
    basic_salary: Joi.number().min(1).required(),
    hra: Joi.number().min(0),
    tax: Joi.number().min(0),
    extra: Joi.number().min(0),
  });

  schema.validateAsync(packet)
    .then((result) => {
      if (result) {
        next();
      }
    })
    .catch((err) => {
      response.status(400)
        .json({
          errorType: 'VALIDATION_ERROR',
          errorMessage: err.details[0].message,
        });
    });
};

exports.validateUpdateSalaryStruct = (request, response, next) => {
  const packet = {
    ...request.body,
    ...request.params,
  };

  const schema = Joi.object({
    sid: Joi.number().integer().min(1),
    salary_structure_name: Joi.string().min(3),
    basic_salary: Joi.number().min(1),
    hra: Joi.number().min(0),
    tax: Joi.number().min(0),
    extra: Joi.number().min(0),
  });

  schema.validateAsync(packet)
    .then((result) => {
      if (result) {
        next();
      }
    })
    .catch((err) => {
      response.status(400)
        .json({
          errorType: 'VALIDATION_ERROR',
          errorMessage: err.details[0].message,
        });
    });
};

exports.validateDeleteSalaryStruct = (request, response, next) => {
  const packet = {
    ...request.params,
  };

  const schema = Joi.object({
    sid: Joi.number().integer().min(1),
  });

  schema.validateAsync(packet)
    .then((result) => {
      if (result) {
        next();
      }
    })
    .catch((err) => {
      response.status(400)
        .json({
          errorType: 'VALIDATION_ERROR',
          errorMessage: err.details[0].message,
        });
    });
};
