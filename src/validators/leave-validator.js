const Joi = require('@hapi/joi');

exports.validateCreateLeave = (request, response, next) => {
  const packet = {
    ...request.body,
  };

  const schema = Joi.object({
    leave_type: Joi.string().min(3).required(),
  });

  schema.validateAsync(packet)
    .then((result) => {
      if (result) {
        next();
      }
    })
    .catch((err) => {
      response.status(400)
        .json({
          errorType: 'VALIDATION_ERROR',
          errorMessage: err.details[0].message,
        });
    });
};

exports.validateDeleteLeave = (request, response, next) => {
  const packet = {
    ...request.params,
  };

  const schema = Joi.object({
    lid: Joi.number().integer().min(1),
  });

  schema.validateAsync(packet)
    .then((result) => {
      if (result) {
        next();
      }
    })
    .catch((err) => {
      response.status(400)
        .json({
          errorType: 'VALIDATION_ERROR',
          errorMessage: err.details[0].message,
        });
    });
};

exports.validateUpdateLeave = (request, response, next) => {
  const packet = {
    ...request.body,
    ...request.params,
  };

  const schema = Joi.object({
    leave_type: Joi.string().min(3).required(),
    lid: Joi.number().integer().min(1),
  });

  schema.validateAsync(packet)
    .then((result) => {
      if (result) {
        next();
      }
    })
    .catch((err) => {
      response.status(400)
        .json({
          errorType: 'VALIDATION_ERROR',
          errorMessage: err.details[0].message,
        });
    });
};
