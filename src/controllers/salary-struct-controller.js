const {
  createSalaryStruct, updateSalaryStructureById, deleteSalaryStructureById, getAllSalaryStructures,
} = require('../services/salary-struct-service.js');

const defaultErrorType = 'INTERNAL_SERVER_ERROR';

const defaultErrorMsg = 'Something went wrong. Please try again later';

exports.createSalaryStruct = (req, res) => {
  createSalaryStruct(req.body)
    .then((salaryStructure) => {
      res.status(201)
        .json(salaryStructure);
    })
    .catch((err) => {
      res.status(err.statusCode || 500)
        .json({
          errorType: err.errorType || defaultErrorType,
          errorMessage: err.errorMessage || defaultErrorMsg,
        });
    });
};

exports.updateSalaryStructureById = (req, res) => {
  updateSalaryStructureById(req.params.sid, req.body)
    .then((updatedSalaryStructure) => {
      res.status(200)
        .json({
          message: updatedSalaryStructure,
        });
    })
    .catch((err) => {
      res.status(err.statusCode || 500)
        .json({
          errorType: err.errorType || defaultErrorType,
          errorMessage: err.errorMessage || defaultErrorMsg,
        });
    });
};

exports.deleteSalaryStructureById = (req, res) => {
  deleteSalaryStructureById(req.params.sid)
    .then((deletedSalaryStructMsg) => {
      res.status(200).json({
        message: deletedSalaryStructMsg,
      });
    })
    .catch((err) => {
      res.status(err.statusCode || 500)
        .json({
          errorType: err.errorType || defaultErrorType,
          errorMessage: err.errorMessage || defaultErrorMsg,
        });
    });
};

exports.getAllSalaryStructures = (req, res) => {
  const defaultItemsPerPage = 10;
  const defaultOffset = 0;
  getAllSalaryStructures(req.query.itemsPerPage || defaultItemsPerPage, req.query.offset || defaultOffset)
    .then((salaryStructures) => {
      res.status(200)
        .json({
          salary_structures: salaryStructures[0],
          metadata: {
            total: salaryStructures[1],
            itemsPerPage: parseInt(req.query.itemsPerPage, 10) || defaultItemsPerPage,
            offset: parseInt(req.query.offset, 10) || defaultOffset,
          },
        });
    })
    .catch((err) => {
      console.log(err);
      res.status(err.statusCode || 500)
        .json({
          errorType: err.errorType || defaultErrorType,
          errorMessage: err.errorMessage || defaultErrorMsg,
        });
    });
};
