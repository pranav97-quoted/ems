const {
  getLeaves, createLeave, deleteLeaveById, updateLeaveById,
} = require('../services/leave-service.js');

const defaultErrorType = 'INTERNAL_SERVER_ERROR';

const defaultErrorMsg = 'Something went wrong. Please try again later';

exports.getLeaves = (req, res) => {
  const defaultItemsPerPage = 10;
  const defaultOffset = 0;
  getLeaves(req.query.itemsPerPage || defaultItemsPerPage, req.query.offset || defaultOffset)
    .then((leaves) => {
      res.status(200)
        .json({
          leaves: leaves[0],
          metadata: {
            total: leaves[1],
            itemsPerPage: parseInt(req.query.itemsPerPage, 10) || defaultItemsPerPage,
            offset: parseInt(req.query.offset, 10) || defaultOffset,
          },
        });
    })
    .catch((err) => {
      res.status(err.statusCode || 500)
        .json({
          errorType: err.errorType || defaultErrorType,
          errorMessage: err.errorMessage || defaultErrorMsg,
        });
    });
};

exports.createLeave = (req, res) => {
  createLeave(req.body)
    .then((leave) => {
      res.status(200)
        .json({
          leave_id: leave.id,
          leave_type: leave.leave_type,
          created_at: leave.created_at,
          updated_at: leave.updated_at,
        });
    })
    .catch((err) => {
      res.status(err.statusCode || 500)
        .json({
          errorType: err.errorType || defaultErrorType,
          errorMessage: err.errorMessage || defaultErrorMsg,
        });
    });
};

exports.deleteLeaveById = (req, res) => {
  deleteLeaveById(req.params.lid)
    .then((deletedLeave) => {
      res.status(200)
        .json({
          status: deletedLeave,
        });
    })
    .catch((err) => {
      res.status(err.statusCode || 500)
        .json({
          errorType: err.errorType || defaultErrorType,
          errorMessage: err.errorMessage || defaultErrorMsg,
        });
    });
};

exports.updateLeaveById = (req, res) => {
  updateLeaveById(req.body.leave_type, req.params.lid)
    .then((updatedLeave) => {
      res.status(200)
        .json({
          id: updatedLeave.id,
          leave_type: updatedLeave.leave_type,
          created_at: updatedLeave.created_at,
          updated_at: updatedLeave.updated_at,
          status: 'Leave updated successfully',
        });
    })
    .catch((err) => {
      res.status(err.statusCode || 500)
        .json({
          errorType: err.errorType || defaultErrorType,
          errorMessage: err.errorMessage || defaultErrorMsg,
        });
    });
};
