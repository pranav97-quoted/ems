const { createSalary, getSalaries } = require('../services/salary-service.js');

const defaultErrorType = 'INTERNAL_SERVER_ERROR';

const defaultErrorMsg = 'Something went wrong. Please try again later';

exports.createSalary = (req, res) => {
  createSalary(req.params.uid, req.body)
    .then((userSalary) => {
      res.status(200)
        .json({
          salary_id: userSalary.id,
          user_id: userSalary.user_id,
          user_salary_struct_id: userSalary.user_salary_struct_id,
          salary_date: userSalary.salary_date,
        });
    })
    .catch((err) => {
      res.status(err.statusCode || 500)
        .json({
          errorType: err.errorType || defaultErrorType,
          errorMessage: err.errorMessage || defaultErrorMsg,
        });
    });
};

exports.getSalaries = (req, res) => {
  const defaultItemsPerPage = 10;
  const defaultOffset = 0;
  getSalaries(req.params.uid, req.query || null, req.query.itemsPerPage || defaultItemsPerPage, req.query.offset || defaultOffset)
    .then((userSalaries) => {
      res.status(200)
        .json({
          user_salaries: userSalaries[0],
          metadata: {
            total: userSalaries[1],
            itemsPerPage: req.query.itemsPerPage || defaultItemsPerPage,
            offset: req.query.offset || defaultOffset,
          },
        });
    })
    .catch((err) => {
      res.status(err.statusCode || 500)
        .json({
          errorType: err.errorType || defaultErrorType,
          errorMessage: err.errorMessage || defaultErrorMsg,
        });
    });
};
