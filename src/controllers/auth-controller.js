const { authService } = require('../services/auth-service.js');

const defaultErrorType = 'INTERNAL_SERVER_ERROR';

const defaultErrorMsg = 'Something went wrong. Please try again later';

exports.auth = (req, res) => {
  authService(req.body).then((users) => {
    req.session.user = {
      userId: users[0].id,
    };
    res.status(200).json({
      user_id: users[0].id,
      message: 'Logging in....',
    });
  }).catch((err) => {
    console.log(err, 'auth error');
    res.status(err.statusCode || 500);
    res.json({
      errorType: err.errorType || defaultErrorType,
      errorMessage: err.errorMessage || defaultErrorMsg,
    });
  });
};

exports.authStatus = (req, res) => {
  if (req.session && req.session.user && req.session.user.userId) {
    res.status(200).json({
      login: true,
      user_id: req.session.user.userId,
    });
  } else {
    res.status(200).json({
      login: false,
    });
  }
};

exports.authLogout = (req, res) => {
  req.session.destroy(() => {
    res.status(200).json({
      login: false,
    });
  });
};
