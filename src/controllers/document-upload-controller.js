const {
  getDocuments, uploadDocument, downloadDocument, deleteDocument,
} = require('../services/document-upload-service.js');

const defaultErrorType = 'INTERNAL_SERVER_ERROR';

const defaultErrorMsg = 'Something went wrong. Please try again later';

exports.getDocuments = (req, res) => {
  const defaultItemsPerPage = 10;
  const defaultOffset = 0;
  getDocuments(req.query.itemsPerPage || defaultItemsPerPage, req.query.offset || defaultOffset, req.params.uid)
    .then((documents) => {
      res.status(200)
        .json({
          documents: documents[0],
          metadata: {
            total: documents[1],
            itemsPerPage: parseInt(req.query.itemsPerPage, 10) || defaultItemsPerPage,
            offset: parseInt(req.query.offset, 10) || defaultOffset,
          },
        });
    })
    .catch((err) => {
      res.status(err.statusCode || 500)
        .json({
          errorType: err.errorType || defaultErrorType,
          errorMessage: err.errorMessage || defaultErrorMsg,
        });
    });
};

exports.uploadDocument = (req, res) => {
  uploadDocument(req.params.uid, req.file)
    .then((result) => {
      res.status(200)
        .json({
          status: result,
        });
    })
    .catch((err) => {
      res.status(err.statusCode || 500)
        .json({
          errorType: err.errorType || defaultErrorType,
          errorMessage: err.errorMessage || defaultErrorMsg,
        });
    });
};

exports.downloadDocument = (req, res) => {
  downloadDocument(req.params.doid, req.params.uid)
    .then((document) => {
      res.download(`./static/users/${req.params.uid}/${document.file_name}`);
    })
    .catch((err) => {
      res.status(err.errorCode || 500)
        .json({
          errorType: err.errorType || defaultErrorType,
          errorMessage: err.errorMessage || defaultErrorMsg,
        });
    });
};

exports.deleteDocument = (req, res) => {
  deleteDocument(req.params.doid, req.params.uid)
    .then((deletedDocument) => {
      res.status(200)
        .json({
          status: deletedDocument,
        });
    })
    .catch((err) => {
      res.status(err.statusCode || 500)
        .json({
          errerrorType: err.errorType || defaultErrorType,
          errorMessage: err.errorMessage || defaultErrorMsg,
        });
    });
};
