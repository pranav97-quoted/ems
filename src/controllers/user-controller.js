const {
  createUser, confirmUser, updateUserById, deleteUserById, getCurrentLoggedInUser
} = require('../services/user-service.js');

const defaultErrorType = 'INTERNAL_SERVER_ERROR';

const defaultErrorMsg = 'Something went wrong. Please try again later';

exports.createUser = (req, res) => {
  createUser(req.body)
    .then((user) => {
      res.status(201)
        .json({
          status: user.status,
        });
    })
    .catch((err) => {
      res.status(err.statusCode || 500).json({
        errorType: err.errorType || defaultErrorType,
        errorMessage: err.errorMessage || defaultErrorMsg,
      });
    });
};

exports.getCurrentLoggedInUser = (req, res) => {
  getCurrentLoggedInUser(req.session.user.userId)
    .then((currentLoggedInUser) => {
      res.status(200)
        .json(currentLoggedInUser);
    })
    .catch((err) => {
      res.status(err.statusCode || 500).json({
        errorType: err.errorType || defaultErrorType,
        errorMessage: err.errorMessage || defaultErrorMsg,
      });
    });
};

exports.tempConfirmUser = (req, res) => {
  res.status(200).json({
    email: req.query.email,
    confirmation_code: req.query.confirmationCode,
  });
};

exports.updateUserById = (req, res) => {
  updateUserById(req.params.uid, req.body)
    .then((updatedUserMessage) => {
      res.status(200)
        .json({
          message: updatedUserMessage,
        });
    })
    .catch((err) => {
      res.status(err.statusCode || 500)
        .json({
          errorType: err.errorType || defaultErrorType,
          errorMessage: err.errorMessage || defaultErrorMsg,
        });
    });
};

exports.confirmUser = (req, res) => {
  confirmUser(req.body.email, req.body.password)
    .then((updatedUserStatus) => {
      if (updatedUserStatus) {
        res.status(200).json({
          status: updatedUserStatus,
        });
      }
    })
    .catch((err) => {
      res.status(err.statusCode || 500)
        .json({
          errorType: err.errorType || defaultErrorType,
          errorMessage: err.errorMessage || defaultErrorMsg,
        });
    });
};

exports.deleteUserById = (req, res) => {
  deleteUserById(req.params.uid)
    .then((deletedUser) => {
      res.status(200).json({
        message: deletedUser,
      });
    })
    .catch((err) => {
      res.status(err.statusCode || 500)
        .json({
          errorType: err.errorType || defaultErrorType,
          errorMessage: err.errorMessage || defaultErrorMsg,
        });
    });
};
