const {
  createDepartment, getDepartments, updateDepartmentById, deleteDepartmentById,
} = require('../services/department-service.js');

const defaultErrorType = 'INTERNAL_SERVER_ERROR';

const defaultErrorMsg = 'Something went wrong. Please try again later';

exports.createDepartment = (req, res) => {
  createDepartment(req.body)
    .then((department) => {
      res.status(201).json({
        department_id: department.id,
        department_name: department.department_name,
        status: 'Department Created Successfully',
      });
    })
    .catch((err) => {
      res.status(err.statusCode || 500)
        .json({
          errorType: err.errorType || defaultErrorType,
          errorMessage: err.errorMessage || defaultErrorMsg,
        });
    });
};

exports.getDepartments = (req, res) => {
  const defaultItemsPerPage = 10;
  const defaultOffset = 0;
  getDepartments(req.query.itemsPerPage || defaultItemsPerPage, req.query.offset || defaultOffset)
    .then((departments) => {
      res.status(200).json({
        departments: departments[0],
        metadata: {
          total: departments[1],
          itemsPerPage: parseInt(req.query.itemsPerPage, 10) || defaultItemsPerPage,
          offset: parseInt(req.query.offset, 10) || defaultOffset,
        },
      });
    })
    .catch((err) => {
      res.status(err.statusCode || 500)
        .json({
          errorType: err.errorType || defaultErrorType,
          errorMessage: err.errorMessage || defaultErrorMsg,
        });
    });
};

exports.updateDepartmentById = (req, res) => {
  updateDepartmentById(req.body.department_name, req.params.did)
    .then((updatedDepartment) => {
      res.status(200).json({
        id: updatedDepartment.id,
        department_name: updatedDepartment.department_name,
        created_at: updatedDepartment.created_at,
        updated_at: updatedDepartment.updated_at,
        status: 'Department updated successfully',
      });
    })
    .catch((err) => {
      res.status(err.statusCode || 500)
        .json({
          errorType: err.errorType || defaultErrorType,
          errorMessage: err.errorMessage || defaultErrorMsg,
        });
    });
};

exports.deleteDepartmentById = (req, res) => {
  deleteDepartmentById(req.params.did)
    .then((deletedDepartment) => {
      res.status(200)
        .json({
          status: deletedDepartment,
        });
    })
    .catch((err) => {
      res.status(err.statusCode || 500)
        .json({
          errorType: err.errorType || defaultErrorType,
          errorMessage: err.errorMessage || defaultErrorMsg,
        });
    });
};
