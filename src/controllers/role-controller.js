const {
  createRole, getRoles, updateRoleById, deleteRoleById,
} = require('../services/role-service.js');

const defaultErrorType = 'INTERNAL_SERVER_ERROR';

const defaultErrorMsg = 'Something went wrong. Please try again later';

exports.createRole = (req, res) => {
  createRole(req.body)
    .then((role) => {
      res.status(201).json({
        role_id: role.id,
        role_type: role.role_type,
        status: 'Role Created Successfully',
      });
    })
    .catch((err) => {
      res.status(err.statusCode || 500)
        .json({
          errorType: err.errorType || defaultErrorType,
          errorMessage: err.errorMessage || defaultErrorMsg,
        });
    });
};

exports.getRoles = (req, res) => {
  const defaultItemsPerPage = 10;
  const defaultOffset = 0;
  getRoles(req.query.itemsPerPage || defaultItemsPerPage, req.query.offset || defaultOffset)
    .then((roles) => {
      res.status(200)
        .json({
          roles: roles[0],
          metadata: {
            total: roles[1],
            itemsPerPage: parseInt(req.query.itemsPerPage, 10) || defaultItemsPerPage,
            offset: parseInt(req.query.offset, 10) || defaultOffset,
          },
        });
    }).catch((err) => {
      res.status(err.statusCode || 500)
        .json({
          errorType: err.errorType || defaultErrorType,
          errorMessage: err.errorMessage || defaultErrorMsg,
        });
    });
};

exports.updateRoleById = (req, res) => {
  updateRoleById(req.body.role_type, req.params.rid)
    .then((updatedRole) => {
      res.status(200).json({
        id: updatedRole.id,
        role_type: updatedRole.role_type,
        created_at: updatedRole.created_at,
        updated_at: updatedRole.updated_at,
        status: 'Role updated successfully',
      });
    })
    .catch((err) => {
      res.status(err.statusCode || 500)
        .json({
          errorType: err.errorType || defaultErrorType,
          errorMessage: err.errorMessage || defaultErrorMsg,
        });
    });
};

exports.deleteRoleById = (req, res) => {
  deleteRoleById(req.params.rid)
    .then((deletedRole) => {
      res.status(200)
        .json({
          status: deletedRole,
        });
    })
    .catch((err) => {
      res.status(err.statusCode || 500)
        .json({
          errorType: err.errorType || defaultErrorType,
          errorMessage: err.errorMessage || defaultErrorMsg,
        });
    });
};
