const {
  createCompany, getCompanies, updateCompanyById, deleteCompanyById,
} = require('../services/company-service.js');

const defaultErrorType = 'INTERNAL_SERVER_ERROR';

const defaultErrorMsg = 'Something went wrong. Please try again later';

exports.createCompany = (req, res) => {
  createCompany(req.body)
    .then((company) => {
      res.status(201)
        .json({
          company_id: company.id,
          company_name: company.company_name,
          status: 'Company Created Successfully',
        });
    })
    .catch((err) => {
      res.status(err.statusCode || 500)
        .json({
          errorType: err.errorType || defaultErrorType,
          errorMessage: err.errorMessage || defaultErrorMsg,
        });
    });
};

exports.getCompanies = (req, res) => {
  const defaultItemsPerPage = 10;
  const defaultOffset = 0;
  getCompanies(req.query.itemsPerPage || defaultItemsPerPage, req.query.offset || defaultOffset)
    .then((companies) => {
      res.status(200)
        .json({
          companies: companies[0],
          metadata: {
            total: companies[1],
            itemsPerPage: parseInt(req.query.itemsPerPage, 10) || defaultItemsPerPage,
            offset: parseInt(req.query.offset, 10) || defaultOffset,
          },
        });
    }).catch((err) => {
      res.status(err.statusCode || 500)
        .json({
          errorType: err.errorType || defaultErrorType,
          errorMessage: err.errorMessage || defaultErrorMsg,
        });
    });
};

exports.updateCompanyById = (req, res) => {
  updateCompanyById(req.body.company_name, req.params.cid)
    .then((updatedCompany) => {
      res.status(200).json({
        id: updatedCompany.id,
        company_name: updatedCompany.company_name,
        created_at: updatedCompany.created_at,
        updated_at: updatedCompany.updated_at,
        status: 'Company updated successfully',
      });
    })
    .catch((err) => {
      res.status(err.statusCode || 500)
        .json({
          errorType: err.errorType || defaultErrorType,
          errorMessage: err.errorMessage || defaultErrorMsg,
        });
    });
};

exports.deleteCompanyById = (req, res) => {
  deleteCompanyById(req.params.cid)
    .then((deletedCompany) => {
      res.status(200).json({
        status: deletedCompany,
      });
    })
    .catch((err) => {
      res.status(err.statusCode || 500)
        .json({
          errorType: err.errorType || defaultErrorType,
          errorMessage: err.errorMessage || defaultErrorMsg,
        });
    });
};
