const sgEmail = require('@sendgrid/mail');

sgEmail.setApiKey(process.env.SENDGRID_API_KEY);

exports.sendEmailConfirmation = (emailHeaders, emailBody) => {
  const msg = {
    to: emailHeaders.to,
    from: emailHeaders.from,
    subject: emailHeaders.subject,
    html: emailBody.html,
  };
  return sgEmail.send(msg)
    .then(() => ({
      status: 'Mail sent successfully',
    })).catch((err) => {
      console.log(err);
    });
};
