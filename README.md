# Employee Management System

An employee management system consists of crucial work-related and important personal information about an employee. In a nutshell, it is an inventory of all employees of an organization. Employees are the strength of any organization, and it is more so in case of growing business. It is crucial to handle this aspect of your business well. A good employee management system can actually make a world of difference to an organization, especially true in case of startups and small businesses, where the focus should be on growing the business more than anything else.


## Tech & Tools
ems is build upon core JavaScript and Node.js
- > JavaScript
- > Visual Studio Code
- > Node.js
- > PostgreSQL
- > Postman API Client


## Installation
- Pre-requisites
    > ems requires Node.js v10+ to run.

    > ems requires PostgreSQL v9+ to run.

    > ems requires Postman API Client for running APIs.
    
    Clone the project by copying below mentioned command into your directory :-

    ```sh
    git clone https://gitlab.com/malik_lakhani106/ems.git
    ```

    Change directory to ems by typing below mentioned command :-

    ```sh
    cd ems
    ```

    Now run below mentioned command to install neccessary dependencies into your project :-

    ```sh
    npm install
    ```

    ## Configuring the Environment

    In order to run this system, first thing is the admin user must have the Send Grid account for email sending and a couple of other environment settings which are described below and this should be configurable in .env file in root directory of this project :-

    ```sh
    PGUSER=database_user_name
    PGHOST=database.server.com
    PGPASSWORD=database_secret_password
    PGDATABASE=database_name
    PGPORT=5432
    SECRET=secret_for_session
    SENDGRID_API_KEY=your_send_grid_api_key
    API_HOST=system_host
    ```

    ## Configuring Database

    After installing the PostgreSQL and configuring the user, one should have to run migration scripts in order to create database and its tables :-

    ```sh
    psql -h localhost -p 5432 -U postgres -d ems -f ems/database/migrations/sql_file.sql
    ```

    ## Important
    Install nodemon as a utility that will monitor for any changes in your source and automatically restart your server. Perfect for development. Install it using below command :-

    ```sh
    npm install -g nodemon
    ```

    ## Creating the Admin User

    After configuring the database and migration files one has to create the admin user in order to use this system :-

    ```sh
    node ems/scripts/initial_seeder.js
    ```

    ## Executing the server

    In order to start the server to work, you can execute the command which is mentioned below :-

    ```sh
    npm start
    ```

    This would execute the code from ems/bin/www directory

## Makefile Commands

1) Display linting for all files

    ```sh
    make all
    ```

2) Autofix fixable errors for linting for all files

    ```sh
    make all_fix
    ```

3) Display linting for staged files

    ```sh
    make lint
    ```

4) Autofix fixable errors for linting in staged files

    ```sh
    make lint_fix
    ```