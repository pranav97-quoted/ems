const bcrypt = require('bcryptjs');

const readline = require('readline');

require('dotenv').config({
  path: '../.env',
});

const { executeQuery, knex } = require('../src/providers/db-connections.js');

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

const getFirstNameFromUser = () => new Promise((resolve) => {
  rl.question('Enter first name : ', (firstName) => {
    resolve(firstName);
  });
});

const getEmailFromUser = () => new Promise((resolve) => {
  rl.question('Enter email address : ', (userEmail) => {
    resolve(userEmail);
  });
});

const getPasswordFromUser = () => new Promise((resolve) => {
  rl.question('Enter password : ', (userPassword) => {
    resolve(userPassword);
  });
});

const createAdminRole = () => {
  const createAdminRoleQuery = knex('roles')
    .insert({ role_type: 'admin' }).returning('id');
  executeQuery(createAdminRoleQuery)
    .then((userRole) => {
      getFirstNameFromUser()
        .then((firstName) => {
          getEmailFromUser()
            .then((email) => {
              getPasswordFromUser()
                .then((password) => {
                  bcrypt.genSalt(10, (err, salt) => {
                    bcrypt.hash(password, salt, (err, hash) => {
                      const createAdminUserQuery = knex('users').insert({
                        first_name: firstName,
                        user_email: email,
                        user_password: hash,
                        is_active: true,
                      }).returning('id');
                      executeQuery(createAdminUserQuery)
                        .then((user) => {
                          const assignAdminRoleQuery = knex('user_role').insert({
                            user_id: user.rows[0].id,
                            role_id: userRole.rows[0].id,
                          }).returning('id');
                          executeQuery(assignAdminRoleQuery)
                            .then((result) => {
                              console.log(result.rows[0].id);
                              rl.close();
                            });
                        });
                    });
                  });
                });
            });
        });
    })
    .catch((err) => {
      console.log(err.detail);
      process.exit(0);
    });
};

rl.on('close', () => {
  process.exit(0);
});

createAdminRole();
